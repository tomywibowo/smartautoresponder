@extends('layouts/app')

@section('container')

<body class="vertical-layout vertical-menu-modern 2-columns fixed-navbar" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <!-- Header -->
    @include('layouts.partials.header')
    <!-- END: Header -->

    <!-- Sidebar -->
    @include('layouts.partials.sidebar')
    <!-- END: Sidebar -->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                @yield('header')
            </div>
            <div class="content-body">
                @yield('content')
            </div>
        </div>
    </div>

    @endsection