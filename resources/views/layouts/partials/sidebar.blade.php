<?php

use Illuminate\Support\Facades\Route;

$routeName = Route::currentRouteName();
?>

<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      <li class=" nav-item">
        <a href="{{ route('home') }}">
          <i class="la la-home"></i>
          <span class="menu-title">{{ __('navigation.dashboard') }}</span>
        </a>
      </li>

      @can('product-read')
      <li class=" nav-item">
        <a href="{{ route('products.index') }}">
          <i class="la la-clipboard"></i>
          <span class="menu-title">{{ __('navigation.product-form') }}</span>
        </a>
      </li>
      @endcan

      @can('contact-read')
      <li class=" nav-item">
        <a href="{{ route('contacts.index') }}">
          <i class="la la-phone-square"></i>
          <span class="menu-title">{{ __('navigation.contact') }}</span>
        </a>
      </li>
      @endcan

      @if(Auth::user()->can('message-read') || Auth::user()->can('broadcast-read'))
      <li class=" nav-item {{ strpos(request()->path(), 'whatsapp') === 0 ? 'open' : '' }}">
        <a href="{{ route('broadcasts.index') }}">
          <i class="la la-whatsapp"></i>
          <span class="menu-title">{{ __('navigation.broadcast.parent') }}</span>
        </a>

        <ul class="menu-content">
          <li class="is-shown">
            <a class="menu-item" href="{{ route('broadcasts.index') }}">
              <i></i>
              <span>{{ __('navigation.broadcast.create') }}</span>
            </a>
          </li>

          <li class="is-shown">
            <a class="menu-item" href="{{ route('broadcasts.history') }}">
              <i></i>
              <span>{{ __('navigation.broadcast.history') }}</span>
            </a>
          </li>

          <li class="is-shown">
            <a class="menu-item" href="{{ route('broadcasts.schedule') }}">
              <i></i>
              <span>{{ __('navigation.broadcast.schedule') }}</span>
            </a>
          </li>
        </ul>
      </li>
      @endif

      @if(Auth::user()->can('whatsapp-read') || Auth::user()->can('kirim-email-read') || Auth::user()->can('google-contact-read'))
      <li class=" nav-item {{ strpos(request()->path(), 'integrate') === 0 ? 'open' : '' }}">
        <a href="{{ route('whatsapp.index') }}">
          <i class="la la-rotate-left"></i>
          <span class="menu-title">{{ __('navigation.integrate') }}</span>
        </a>

        <ul class="menu-content">
          @can('whatsapp-read')
          <li class="is-shown">
            <a class="menu-item" href="{{ route('whatsapp.index') }}">
              <i></i>
              <span>{{ __('navigation.whatsapp') }}</span>
            </a>
          </li>
          @endcan

          @can('kirim-email-read')
          <li class="is-shown">
            <a class="menu-item" href="{{ route('kirim-email.index') }}">
              <i></i>
              <span>{{ __('navigation.kirim.email') }}</span>
            </a>
          </li>
          @endcan

          @can('google-contact-read')
          <li class="is-shown">
            <a class="menu-item" href="{{ route('google-contact.index') }}">
              <i></i>
              <span>{{ __('navigation.google-contact') }}</span>
            </a>
          </li>
          @endcan
        </ul>
      </li>
      @endif

      <li class="navigation-header">
        <span>{{ __('navigation.other') }}</span>
      </li>

      @if(Auth::user()->can('users-read') || Auth::user()->can('role-read') || Auth::user()->can('permission-read'))
      <li class=" nav-item
        {{ strpos($routeName, 'users') === 0 ? 'open' : '' }}
        {{ strpos($routeName, 'roles') === 0 ? 'open' : '' }}
        {{ strpos($routeName, 'permissions') === 0 ? 'open' : '' }}
        ">
        <a href="{{ route('home') }}">
          <i class="la la-user"></i>
          <span class="menu-title">{{ __('navigation.user') }}</span>
        </a>

        <ul class="menu-content">
          @can('users-read')
          <li class="is-shown">
            <a class="menu-item" href="{{ route('users.index') }}">
              <i></i>
              <span>{{ __('navigation.user.list') }}</span>
            </a>
          </li>
          @endcan

          @can('role-read')
          <li class="is-shown">
            <a class="menu-item" href="{{ route('roles.index') }}">
              <i></i>
              <span>{{ __('navigation.role') }}</span>
            </a>
          </li>
          @endcan

          @can('permission-read')
          <li class="is-shown">
            <a class="menu-item" href="{{ route('permissions.index') }}">
              <i></i>
              <span>{{ __('navigation.permission') }}</span>
            </a>
          </li>
          @endcan
        </ul>
      </li>
      @endcan

      @can('setting-read')
      <li class=" nav-item">
        <a href="{{ route('settings.index') }}">
          <i class="la la-cogs"></i>
          <span class="menu-title">{{ __('navigation.setting') }}</span>
        </a>
      </li>
      @endcan

      <li class=" nav-item">
        <a href="{{ route('help.index') }}">
          <i class="la la-question-circle"></i>
          <span class="menu-title">{{ __('navigation.help') }}</span>
        </a>
      </li>
    </ul>
  </div>
</div>
