<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-dark navbar-shadow">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mobile-menu d-lg-none mr-auto">
                    <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#">
                        <i class="ft-menu font-large-1"></i>
                    </a>
                </li>

                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="index.html">
                        <img src="https://s3.us-east-2.amazonaws.com/upload-icon/uploads/icons/png/17633534451561032646-256.png" width="45px" />
                    </a>
                </li>

                <li class="nav-item d-none d-lg-block nav-toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                        <i class="toggle-icon ft-toggle-right font-medium-3 white" data-ticon="ft-toggle-right"></i>
                    </a>
                </li>

                <li class="nav-item d-lg-none">
                    <a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile">
                        <i class="la la-ellipsis-v"></i>
                    </a>
                </li>
            </ul>
        </div>

        <div class="navbar-container content">
            <div class="collapse navbar-collapse" id="navbar-mobile">
                <ul class="nav navbar-nav ml-auto float-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                            <span class="mr-1 user-name text-bold-700">{{ Auth::user()->name }}</span>
                            <span class="avatar avatar-online">
                                <img src="{{ asset('assets/images/portrait/small/avatar-s-19.png') }}" alt="avatar">
                                <i></i>
                            </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ route('profile') }}"><i class="ft-user"></i> {{ __('account.profile') }}</a>

                            <div class="dropdown-divider"></div>

                            <a class="dropdown-item" href="javascript::void()" onclick="$('#logout').submit()">
                                <i class="ft-power"></i> {{ __('auth.logout') }}

                                <form method="post" action="{{ route('logout') }}" id="logout">
                                    @csrf
                                </form>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>