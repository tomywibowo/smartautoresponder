<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Plugins -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/fontawesome/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/fontawesome/css/font-awesome-animation.css') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <style>
        .product-theme-app-wrapper {
            margin: 0 auto;
            padding-top: 1em;
            max-width: 53em;
            min-height: 100%;
        } 

        .product-theme-app-header {
            text-align: center;
            font-size: .8em;
        }

        @media screen and (min-width: 768px) {
            .product-theme-app-header {
                font-size: 1em;
            }
        }

        .product-theme-app-header__title {
            margin-top: .5em;
            color: #000;
            color: #252525;
            font-size: 2.2em;
            font-weight: 700;
        }

        .procuct-theme-app-header__description {
            margin-bottom: 2rem;
            color: #949494;
            font-size: 1.2em;
        }

        .product-theme-checkout-body {
            background-color: #fff;
            -webkit-box-shadow: 0 0 0.25em rgb(0 0 0 / 30%);
            box-shadow: 0 0 0.25em rgb(0 0 0 / 30%);
            border-radius: 3px;
            overflow: hidden;
        }

        .product-theme-checkout-body__form {
            margin-bottom: 20px;
            padding: 2em 1em;
            padding-top: 20px;
        }

        @media screen and (min-width: 481px) {
            .product-theme-checkout-body__form {
                padding: 2em 1.25em;
            }
        }

        @media screen and (min-width: 768px) {
            .product-theme-checkout-body__form {
                max-width: 62.5%;
            }
        }

        @media screen and (min-width: 961px) {
            .product-theme-checkout-body__form {
                padding: 2rem;
            }
        }
        
        .product-theme-image-container {
            margin-bottom: 15px;
            padding: 0;
            max-width: 90%;
            margin-left: auto;
            margin-right: auto;
        }

        @media screen and (min-width: 961px) {
            .product-theme-image-container {
                padding: 0 55px;
                margin-bottom: 25px;
            }
        }

        .product-theme-checkout-body__form .product-theme-image-container img {
            width: 100%;
            margin: 0 auto;
        }

        .product-theme-contact-information {
            margin-bottom: 40px;
        }

        .product-theme-contact-information-header {
            margin-bottom: 20px;
        }

        .product-theme-section-title {
            position: relative;
            color: #252525;
            font-size: 20px;
            font-weight: 700;
        }

        .product-theme-contact-information-header.product-theme-section-title {
            position: relative;
            color: #252525;
            font-size: 20px;
            font-weight: 700;
        }

        .product-theme-contact-information-header.product-theme-section-title span {
            position: relative;
            padding-right: 5px;
            background-color: #fff;
            z-index: 1;
        }

        .product-theme-payment-information  {
            margin-bottom: 40px;
        }
    </style>

    <title>Hello, world!</title>
  </head>
  <body>
      <div class="container">
        <div class="product-theme-app-wrapper" data-select2-id="6">
            <div class="product-theme-app-header">
                <h1 class="product-theme-app-header__title">OrderOnline Personal</h1>
                <h2 class="procuct-theme-app-header__description">Tingkatkan Penjualan dan Konversi dengan OrderOnline</h2>
            </div>
            <form class="form orderonline-embed-form contact-form" data-username="buy" data-product-slug="personal" data-mode="all" id="form-603476c89d8e8e300c640833" novalidate="novalidate">
                <div class="container-fluid product-theme-checkout-body">
                    <div class="row">
                        <div class="col product-theme-checkout-body__form">
                            <div class="image-container">
                                <div class="row">
                                    <div class="col">
                                        <img src="https://cdn.orderonline.id/img/seal_secure_id.png" class="img-fluid" alt="Transaksi Aman">
                                    </div>
                                    <div class="col">
                                        <img src="https://cdn.orderonline.id/img/seal_money_back_id.png" class="img-fluid" alt="money_back_id">
                                    </div>
                                </div>
</head>

<body id="template-main-background" class="bg-gray-100">
    <div class="container my-5">
        <h1 class="text-center sm:text-medium" id="product_name">{{ $product ? $product->name : '' }}</h1>
        <h5 class="text-secondary text-center d-none" id="template-header-tagline"></h5>

        <div class="d-flex justify-center">
            <div class="card w-12/12 md:w-10/12 mt-5">
                <div class="row">
                    <div class="col-md-7 p-5">
                        <div class="d-flex justify-content-center align-items-center" style="gap: 1.7rem">
                            <div class="d-flex align-items-center">
                                <i class="text-gray-300 zmdi zmdi-lock zmdi-hc-4x mr-3.5"></i>
                                <span class="text-base font-semibold text-gray-400">SECURE <br> CHECKOUT</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <i class="text-gray-300 zmdi zmdi-thumb-up zmdi-hc-4x mr-3.5"></i>
                                <span class="text-base font-semibold text-gray-400" id="template-guarantee-el">MONEY BACK GUARANTEE</span>
                            </div>
                            <div class="product-theme-contact-information">
                                <div class="product-theme-contact-information-header section-title">
                                    <span>Data Penerima:</span>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="name" class="ooef-field form-control ooef-field-name valid" id="field-name" placeholder="Nama Anda" maxlength="160" data-label="Nama" required="" aria-required="true" aria-invalid="false">
                                </div>
                                <div class="form-group  ">
                                    <input type="tel" name="phone" class="ooef-field form-control ooef-field-phone valid" id="field-phone" placeholder="No. WhatsApp Anda" maxlength="160" data-label="No Handphone/WhatsApp" required="" aria-required="true" aria-invalid="false">
                                </div>
                                <div class="form-group  ">
                                    <input type="email" name="email" class="ooef-field form-control ooef-field-email valid" id="field-email" placeholder="Email Anda" maxlength="160" data-label="Email" required="" aria-required="true" aria-invalid="false">
                                </div>
                            </div>

                            <div class="product-theme-payment-information  ">
                                <div class="payment-information-header section-title">
                                    <span>Metode Pembayaran:</span>
                                </div>
                                <div class="ooef-payment-options">
                                    <div class="ooef-payment-option">
                                        <div class="ooef-payment-option-title">
                                            <label class="custom-control custom-radio">
                                                <input type="radio" name="ooef_payment_method" class="ooef-payment-method custom-control-input" value="bank_transfer" checked="">
                                                <div class="custom-control-label">
                                                    <img src="https://cdn.orderonline.id/icons/payment-bank_transfer.png" alt="bank_transfer">
                                                    <span>Bank Transfer</span>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="ooef-payment-option-title">
                                        <label class="custom-control custom-radio">
                                            <input type="radio" name="ooef_payment_method" class="ooef-payment-method custom-control-input ooef-payment-channels" data-channel="credit_card" value="instant">
                                            <div class="custom-control-label">
                                                <img src="https://cdn.orderonline.id/icons/payment-channel-credit_card.png" alt="credit_card">
                                                <span>Credit Card</span>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="ooef-payment-option">
                                    <div class="ooef-payment-option-title">
                                        <label class="custom-control custom-radio">
                                            <input type="radio" name="ooef_payment_method" class="ooef-payment-method custom-control-input ooef-payment-channels" data-channel="bca_va" value="instant">
                                            <div class="custom-control-label">
                                                <img src="https://cdn.orderonline.id/icons/payment-channel-bca_va.png" alt="bca_va">
                                                <span>BCA Virtual Account</span>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="ooef-payment-option">
                                    <div class="ooef-payment-option-title">
                                        <label class="custom-control custom-radio">
                                            <input type="radio" name="ooef_payment_method" class="ooef-payment-method custom-control-input ooef-payment-channels" data-channel="bni_va" value="instant">
                                            <div class="custom-control-label">
                                                <img src="https://cdn.orderonline.id/icons/payment-channel-bni_va.png" alt="bni_va">
                                                <span>BNI Virtual Account</span>
                                            </div>
                                        </label>
                                        
                                    </div>
                                </div>
                                <div class="ooef-payment-option selected">
                                    <div class="ooef-payment-option-title">
                                        <label class="custom-control custom-radio">
                                            <input type="radio" name="ooef_payment_method" class="ooef-payment-method custom-control-input ooef-payment-channels" data-channel="gopay" value="instant">
                                            <div class="custom-control-label">
                                                <img src="https://cdn.orderonline.id/icons/payment-channel-gopay.png" alt="gopay">
                                                <span>GoPay</span>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bump-information ">
                            <div class="bump-actions d-flex">
                                <div class="form-arrow">
                                    <img src="https://cdn.orderonline.id/arrow.gif" alt="">
                                </div>
                                <label class="bump-check m-0">
                                    <div class="custom-control custom-checkbox large white">
                                        <input type="checkbox" class="custom-control-input ooef-check-bump">
                                        <div class="custom-control-label">OrderOnline Personal (1 Tahun)</div>
                                    </div>
                                </label>
                            </div>
                            <div class="bump-body">
                                <div class="bump-title">
                                    GRATIS biaya langganan 2 BULAN jika berlangganan langsung selama 1 tahun
                                </div>
                                            <div class="bump-description">
                                    Harga normal OrderOnline Personal 1 tahun adalah Rp1.788.000. Namun jika Anda berlangganan langsung selama 1 tahun, Anda mendapatkan potongan biaya langganan 2 bulan, sehingga cukup bayar Rp1.499.000 saja. Silahkan centang checkbox diatas!
                            <div id="preview_bumps" class="col-12 mt-3 is-product-preview 
                                @if( $product )
                                    @if( !$product->bump )
                                        d-none
                                    @endif
                                @else
                                    d-none
                                @endif
                            ">
                                <div id="bumps_components">
                                    <div class="bumps-box">
                                        <div class="bumps-box-title">
                                            <i class="far fa-hand-point-right animated faa-flash" style="color:#FF0000;font-size:22px"></i>
                                            <input type="checkbox" name="bumps" id="bumps" />
                                            <span id="create_bump-bump_title">
                                                @if( $product )
                                                @if( $product->bump )
                                                {{ $product->bump->title }}
                                                @else
                                                Write the title bumps
                                                @endif
                                                @else
                                                Write the title bumps
                                                @endif
                                            </span>
                                        </div>
                                        <div class="bumps-box-content">
                                            <p class="subtitle">
                                                <span id="create_bump-product_name">
                                                    @if( $product )
                                                    @if( $product->bump )
                                                    {{ $product->bump->name }}
                                                    @else
                                                    Bump Product Name
                                                    @endif
                                                    @else
                                                    Bump Product Name
                                                    @endif
                                                </span>
                                                <span id="create_bump-product_price">
                                                    @if( $product )
                                                    @if( $product->bump )
                                                    Rp. {{ number_format($product->bump->price, 0, '.', '.') }}
                                                    @else
                                                    Price
                                                    @endif
                                                    @else
                                                    Price
                                                    @endif
                                                </span>
                                            </p>
                                            <p id="create_bump-bump_description">
                                                @if( $product )
                                                @if( $product->bump )
                                                {{ $product->bump->description }}
                                                @else
                                                Description for product bump. Please write in here.
                                                @endif
                                                @else
                                                Description for product bump. Please write in here.
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="oo-captcha-container hidden">
                            <div class="oo-g-recaptcha" data-form-id="form-603476c89d8e8e300c640833"></div>
                        </div>
                        <div class="checkout-actions ">
                                <button type="submit" class="btn btn-block-lg btn-complete-order ooef-submit-order orange  " id="btn-complete-order">
                                <span class="submit-text">PESAN SEKARANG</span>
                                <span class="submit-loader hidden"></span>
                                <svg aria-hidden="true" data-prefix="far" data-icon="arrow-alt-circle-right" class="submit-arrow icon svg-inline--fa fa-arrow-alt-circle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zm-448 0c0-110.5 89.5-200 200-200s200 89.5 200 200-89.5 200-200 200S56 366.5 56 256zm72 20v-40c0-6.6 5.4-12 12-12h116v-67c0-10.7 12.9-16 20.5-8.5l99 99c4.7 4.7 4.7 12.3 0 17l-99 99c-7.6 7.6-20.5 2.2-20.5-8.5v-67H140c-6.6 0-12-5.4-12-12z"></path></svg>
                            </button>
                            <div class="checkout-secure-text hidden">100% safe &amp; secure</div>
                            <div class="btn-stock-empty btn btn-block-lg disabled hidden">Stok Produk Kosong</div>
                        </div>
                    </div>
                    <div class="col checkout-body__sidebar">
                        <div class="product-image-slider">
                            <div class="slick-item featherlight-image" href="https://cdn.orderonline.id/uploads/4521001614268094735-large.png" data-featherlight="image">
                                <img src="https://cdn.orderonline.id/uploads/4521001614268094735.png" alt="OrderOnline Personal">
                            </div>
                        </div>
                        <div class="product-description">
                        </div>


            <div class="product-features">
                            <div class="product-features-header section-title">
                        <span>Yang Anda Dapatkan:</span>
                    </div>
                                    <div class="product-feature-item  d-flex">
                        <div class="product-feature-item__mark mr-1">
                            
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="mark-icon svg-inline--fa fa-check"><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z" class=""></path></svg>
                        </div>
                        <div class="product-feature-item__content">100% web-based software - Anda tinggal login lewat browser, tidak perlu install atau download apapun.</div>
                    </div>
                            <div class="product-feature-item  d-flex">
                        <div class="product-feature-item__mark mr-1">
                            
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="mark-icon svg-inline--fa fa-check"><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z" class=""></path></svg>
                        </div>
                        <div class="product-feature-item__content">Gratis support dan updates.</div>
                    </div>
                            <div class="product-feature-item  d-flex">
                        <div class="product-feature-item__mark mr-1">
                            
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="mark-icon svg-inline--fa fa-check"><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z" class=""></path></svg>
                        </div>
                        <div class="product-feature-item__content">Tutorial lengkap &amp; komunitas Facebook group.</div>
                    </div>
                    </div>


            <div class="order-summary  ">
            <div class="order-summary-header">RINCIAN PESANAN:</div>
            <div class="product-price clearfix">
                <div class="product-price__name float-left summary-product-name">OrderOnline Personal</div>
                                            <div style="display: block;">
                            <div class="product-price-tag float-right">
                                <div class="product-price__price__currency float-left"></div>
                                <div class="product-price__price float-right lined-price summary-product-lined-price">Rp199.000</div>
                    <div class="col-md-5 d-flex align-items-stretch">
                        <div class="bg-gray-200 d-flex flex-column align-items-center p-4 w-100">
                            <div id="product_images_wrapper" class="main-carousel w-100">
                                @if( $product )
                                @foreach ($product->images as $product_image)
                                <div class="carousel-cell w-100 ">
                                    <div style="margin-top: 100%"></div>
                                    <img src="{{ asset($product_image->image_url) }}" alt="" class="img-thumbnail img-fluid position-absolute inset-0">
                                </div>
                                @endforeach
                                @endif
                            </div>
                            <br>
                            <p class="text-base text-center mt-2.5 font-medium text-gray-600 px-5" id="paragraph-product-description"></p>
                            <div class="align-self-start mt-3 w-100">
                                <h3>What You Get</h3>
                                <ul class="list-style-none" id="product_preview-list_features_wrapper">
                                </ul>
                                <div class="bg-white py-2 px-3 mt-4 rounded border-2 border-blue-300">
                                    <p>Order's Summary</p>
                                    <div class="row d-flex">
                                        <div class="col-6 align-self-end" id="product_name">
                                            {{ $product ? $product->name : '' }}
                                        </div>
                                        <div class="col-6 text-right d-flex flex-column">
                                            <div id="product_regular_price" class="text-right">
                                                @if( $product )
                                                @if( $product->pricing )
                                                {!! $product->pricing->is_sale_price ? '<strike class="text-red-600 text-xs">' . 'Rp. ' . number_format($product->pricing->regular_price, 0, '.', '.') . '</strike>' : 'Rp. ' . number_format($product->pricing->regular_price, 0, '.', '.') !!}
                                                @endif
                                                @endif
                                            </div>
                                            <div id="product_sale_price" class="text-right">
                                                @if( $product )
                                                @if( $product->pricing )
                                                {{ $product->pricing->is_sale_price ? 'Rp. ' . number_format($product->pricing->regular_price, 0, '.', '.') : '' }}
                                                @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mt-4">
                                    <label for="coupon_code" class="font-medium mb-2">Enter Coupon Code Number</label>
                                    <input type="text" name="coupon_code" id="coupon_code" class="form-control">
                                </div>
                            </div>
                        </div>
                            
                <div class="product-price-tag order-summary-item-normal float-right">
                    <div class="product-price__price float-right summary-product-price">Rp 149.000</div>
                </div>
            </div>

            <div class="order-summary-multiple-variations"></div>


                    <div class="product-price clearfix">
                    <div class="product-price__name float-left summary-product-bump"></div>
                    <div class="product-price-tag float-right">
                        <div class="product-price__price__currency float-left"></div>
                        <div class="product-price__price float-right summary-product-bump-price"></div>
                    </div>
                </div>




            <div class="product-price discount-summary-container order-summary-item-discount">
                <div class="product-price__name discount-summary-text">
                                    Diskon
                            </div>
                <div class="product-price__price discount-summary-text discount-summary-value">-Rp0</div>
            </div>

                    <div class="product-price clearfix">
                    <div class="product-price-tag float-left unique-code-summary-text">
                                            Kode Unik
                                    </div>
                    <div class="product-price__price float-right unique-code-summary-text summary-unique-code-value">Rp0</div>
                </div>

            <div class="total-price product-price clearfix">
                <div class="product-price__name float-left">Total</div>
                <div class="product-price-tag float-right">
                    <div class="product-price__price__currency float-left"></div>
                    <div class="product-price__price float-right summary-product-total-price">Rp149.000</div>
                </div>
            </div>
            </div>


            <div class="coupon-code  hidden">
            <div class="coupon-header">
                            GUNAKAN KODE KUPON:
                    </div>
            <div class="form-group">
                <div class="coupon-container">
                    <input type="text" name="coupon" class="form-control coupon-field ooef-field-coupon" autocomplete="nope">
                    <button type="button" class="coupon-button coupon-apply-button">Apply</button>
                    <button type="button" class="coupon-button coupon-remove-button">
                        
                        
                        <span class="coupon-apply-button-remove-check">✔</span>
                        <span class="coupon-apply-button-remove-times">✖</span>
                    </button>
                    <button type="button" class="coupon-button coupon-remove-button-failed">
                        
                        <span class="coupon-apply-button-failed-remove-times">✖</span>
                    </button>
                </div>
                <div class="coupon-error">This coupon does not exist.</div>
            </div>
            </div>

                    <div class="visible-md">
                    
                    <div class="checkout-actions ">
                    <button type="submit" class="btn btn-block-lg btn-complete-order ooef-submit-order orange  " id="btn-complete-order">
                    <span class="submit-text">PESAN SEKARANG</span>
                    <span class="submit-loader hidden"></span>
                    <svg aria-hidden="true" data-prefix="far" data-icon="arrow-alt-circle-right" class="submit-arrow icon svg-inline--fa fa-arrow-alt-circle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M504 256C504 119 393 8 256 8S8 119 8 256s111 248 248 248 248-111 248-248zm-448 0c0-110.5 89.5-200 200-200s200 89.5 200 200-89.5 200-200 200S56 366.5 56 256zm72 20v-40c0-6.6 5.4-12 12-12h116v-67c0-10.7 12.9-16 20.5-8.5l99 99c4.7 4.7 4.7 12.3 0 17l-99 99c-7.6 7.6-20.5 2.2-20.5-8.5v-67H140c-6.6 0-12-5.4-12-12z"></path></svg>
                </button>
                <div class="checkout-secure-text hidden">100% safe &amp; secure</div>
                <div class="btn-stock-empty btn btn-block-lg disabled hidden">Stok Produk Kosong</div>
            </div>
                </div>
            </div>
                            </div>

                
                    </div>
            </form>


            
            <div class="app-footer">
            <div class="footer-title">
        Powered by
        <img src="https://cdn.orderonline.id/logo_200_x_40.png" class="img-fluid" alt="OrderOnline">
    </div>
    <div class="footer-copyright">Copyright © 2021</div>
</div>
        </div>
      </div>
    <h1>Hello, world!</h1>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

    <!-- Option 2: jQuery, Popper.js, and Bootstrap JS
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
    -->
  </body>
</body>

</html>