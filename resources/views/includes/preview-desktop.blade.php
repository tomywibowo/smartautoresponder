<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Plugins -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/fontawesome/css/all.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/fonts/fontawesome/css/font-awesome-animation.css') }}">
    
    <!-- Bootstrap CSS -->
    <style>
        * {
            font-family: 'Poppins', sans-serif;
        }

        #bumps_components {
            background: #FFFFC9;
            padding: 10px;
        }

        #bumps_components .bumps-box {
            border: 3px dashed #FF0000;
            padding: 10px;
            min-height: 100px;
        }

        #bumps_components .bumps-box .bumps-box-title {
            background: #FFB33C;
            padding: 8px;
        }

        #bumps_components .bumps-box .bumps-box-title input {
            border: none;
            margin-right: 5px;
        }

        #bumps_components .bumps-box .bumps-box-title span {
            font-weight: bold;
            color: #000;
        }

        #bumps_components .bumps-box .bumps-box-content p.subtitle {
            color: #FF0000;
            font-size: 14px;
            font-weight: bold;
            margin: 8px 0 12px;
        }

        #bumps_components .bumps-box .bumps-box-content p {
            color: #000;
            font-size: 12px;
            margin: 0;
            padding: 0;
        }

        .bg-gray-100 {
            background-color: rgba(243, 244, 246, 1) !important;	
        }

        .text-gray-300 {
            color: rgba(209, 213, 219, 1) !important;
        }

        .text-gray-400 {
            color: rgba(156, 163, 175, 1) !important;
        }
    </style>

    <title>Hello, world!</title>
  </head>
  <div id="template-main-background" class="bg-gray-100">
    <div class="container">
        <h1 class="text-center" id="product_name">{{ $product ? $product->name : '' }}</h1>
        <h5 class="text-secondary text-center d-none" id="template-header-tagline"></h5>

        <div class="d-flex justify-center">
            <div class="card w-75 mx-auto mt-3">
                <div class="row">
                    <div class="col-md-7 p-5">
                        <div class="d-flex justify-content-center align-items-center" style="gap: 1.7rem">
                            <div class="d-flex align-items-center">
                                <i class="text-gray-300 zmdi zmdi-lock zmdi-hc-4x mr-3.5"></i>
                                <span class="text-base font-semibold text-gray-400">SECURE <br> CHECKOUT</span>
                            </div>
                            <div class="d-flex align-items-center">
                                <i class="text-gray-300 zmdi zmdi-thumb-up zmdi-hc-4x mr-3.5"></i>
                                <span class="text-base font-semibold text-gray-400" id="template-guarantee-el">MONEY BACK GUARANTEE</span>
                            </div>
                        </div>
                        <div class="mt-5">
                            <h5 class="font-semibold">CONTACT INFORMATION</h5>
                            <form action="" class="mt-4">
                                <div class="form-group mb-3 required">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Nama">
                                </div>
                                <div class="form-group mb-3 required">
                                    <input type="text" name="number_phone" id="number_phone" class="form-control" placeholder="No. Whatsapp">
                                </div>
                                <div class="form-group mb-3 d-none">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Masukan Email">
                                </div>
                                <div class="form-group mb-3 d-none">
                                    <input type="text" name="note" id="note" class="form-control" placeholder="Masukan Catatan">
                                </div>
                                <div class="form-group mb-3 d-none">
                                    <textarea name="address" class="form-control" id="address" cols="30" rows="5" placeholder="Alamat Lengkap"></textarea>
                                </div>
                                <div class="form-group mb-3 d-none">
                                    <select class="form-select" aria-label="Pilih Provinsi" name="province" id="province">
                                        <option selected disabled>Pilih Provinsi</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="form-group mb-3 d-none">
                                    <select class="form-select" aria-label="Pilih Provinsi" name="city" id="city">
                                        <option selected disabled>Pilih Kota/Kabupaten</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="form-group mb-3 d-none">
                                    <select class="form-select" aria-label="Pilih Provinsi" name="district" id="district">
                                        <option selected disabled>Pilih Kecamatan</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                            </form>
                            <div class="d-flex align-items-center mt-4">
                                <h5 class="font-semibold w-100">PAYMENT INFORMATION : </h5>
                                <hr class="h-1 bg-gray-600 w-100">
                            </div>
                            <div class="form-check mt-1.5 bank-transfer-wrapper is-product-preview 
                                @if( $product )
                                    @if( !$product->payments )
                                        d-none
                                    @endif
                                @else
                                    d-none
                                @endif
                            ">
                                <input class="form-check-input" type="radio" value="1" name="bank_transfer" id="bank_transfer" checked>
                                <label class="form-check-label" for="bank_transfer">
                                    Bank Transfer
                                </label>
                            </div>
                            <div id="preview_bumps" class="col-12 mt-3 is-product-preview 
                                @if( $product )
                                    @if( !$product->bump )
                                        d-none
                                    @endif
                                @else
                                    d-none
                                @endif
                            ">
                                <div id="bumps_components">
                                    <div class="bumps-box">
                                        <div class="bumps-box-title">
                                            <i class="far fa-hand-point-right animated faa-flash" style="color:#FF0000;font-size:22px"></i>
                                            <input type="checkbox" name="bumps" id="bumps" />
                                            <span id="create_bump-bump_title">
                                                @if( $product )
                                                    @if( $product->bump )
                                                        {{ $product->bump->title }}
                                                    @else
                                                        Write the title bumps
                                                    @endif
                                                @else
                                                    Write the title bumps
                                                @endif
                                            </span>
                                        </div>
                                        <div class="bumps-box-content">
                                            <p class="subtitle">
                                                <span id="create_bump-product_name">
                                                    @if( $product )
                                                        @if( $product->bump )
                                                            {{ $product->bump->name }}
                                                        @else
                                                            Bump Product Name
                                                        @endif
                                                    @else
                                                        Bump Product Name
                                                    @endif
                                                </span> 
                                                <span id="create_bump-product_price">
                                                    @if( $product )
                                                        @if( $product->bump )
                                                            Rp. {{ number_format($product->bump->price, 0, '.', '.') }}
                                                        @else
                                                            Price
                                                        @endif
                                                    @else
                                                        Price
                                                    @endif
                                                </span>
                                            </p>
                                            <p id="create_bump-bump_description">
                                                @if( $product )
                                                    @if( $product->bump )
                                                        {{ $product->bump->description }}
                                                    @else
                                                        Description for product bump. Please write in here.
                                                    @endif
                                                @else
                                                    Description for product bump. Please write in here.
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button id="product_template-button_order_now" class="bg-yellow-500 w-100 d-flex align-items-center justify-content-center font-bold py-3 text-white text-lg mt-4 rounded-md text-center">Pesan Sekarang! <img src="{{ asset('icons/arrow-right-circle.svg') }}" class="ml-1.5" alt="Arrow Right Circle"> </button>
                            <p class="text-center font-medium mt-4 text-gray-400">100% safe & secure</p>
                        </div>
                    </div>
                    <div class="col-md-5 d-flex align-items-stretch">
                        <div class="bg-gray-200 d-flex flex-column align-items-center p-4">
                            <div id="product_images_wrapper" class="main-carousel w-100">
                                @if( $product )
                                    @foreach ($product->images as $product_image)
                                        <div class="carousel-cell w-100 ">
                                            <div style="margin-top: 100%"></div>
                                            <img src="{{ asset($product_image->image_url) }}" alt="" class="img-thumbnail img-fluid position-absolute inset-0">
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <br>
                            <p class="text-base text-center mt-2.5 font-medium text-gray-600 px-5" id="paragraph-product-description"></p>
                            <div class="align-self-start mt-3 w-100">
                                <h3>What You Get</h3>
                                <ul class="list-style-none" id="product_preview-list_features_wrapper">
                                </ul>
                                <div class="bg-white py-2 px-3 mt-4 rounded border-2 border-blue-300">
                                    <p>Order's Summary</p>
                                    <div class="row d-flex">
                                        <div class="col-6 align-self-end" id="product_name">
                                            {{ $product ? $product->name : '' }}
                                        </div>
                                        <div class="col-6 text-right d-flex flex-column">
                                            <div id="product_regular_price" class="text-right">
                                                @if( $product )
                                                    @if( $product->pricing )
                                                        {!! $product->pricing->is_sale_price ? '<strike class="text-red-600 text-xs">' . 'Rp. ' . number_format($product->pricing->regular_price, 0, '.', '.') . '</strike>' :  'Rp. ' . number_format($product->pricing->regular_price, 0, '.', '.') !!}
                                                    @endif
                                                @endif
                                            </div>
                                            <div id="product_sale_price" class="text-right">
                                                @if( $product )
                                                    @if( $product->pricing )
                                                        {{ $product->pricing->is_sale_price ? 'Rp. ' . number_format($product->pricing->regular_price, 0, '.', '.') : '' }}
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mt-4">
                                    <label for="coupon_code" class="font-medium mb-2">Enter Coupon Code Number</label>
                                    <input type="text" name="coupon_code" id="coupon_code" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Plugins -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->

    <script>
        $('.main-carousel').flickity({
            autoPlay: true,
            wrapAround: true
        });
    </script>

</div>
</html>