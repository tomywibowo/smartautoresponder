@extends('layouts.blank')

@section('content')
<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-md-4 col-10 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                            <div class="card-header border-0">
                                <div class="card-title text-center">
                                    <img src="https://s3.us-east-2.amazonaws.com/upload-icon/uploads/icons/png/17633534451561032646-256.png" width="80px" />
                                    <h3 class="text-success"><strong>Smartauto Responder</strong></h3>
                                </div>
                            </div>
                            <div class="card-content">
                                <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
                                    <span>{{ __('auth.login-heading') }}</span>
                                </p>
                                <div class="card-body">
                                    <form class="form-horizontal" method="post" action="{{ route('login') }}">
                                        @csrf

                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" placeholder="{{ __('auth.username-label') }}">
                                            <div class="form-control-position">
                                                <i class="ft-user"></i>
                                            </div>

                                            @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </fieldset>

                                        <fieldset class="form-group position-relative has-icon-left">
                                            <input type="password" class="form-control" name="password" id="password" placeholder="{{ __('auth.password-label') }}">
                                            <div class="form-control-position">
                                                <i class="la la-key"></i>
                                            </div>
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </fieldset>

                                        <div class="form-group row">
                                            <div class="col-md-6 col-12 text-center text-sm-left">
                                                <fieldset>
                                                    <input type="checkbox" id="remember-me" class="chk-remember">
                                                    <label for="remember-me"> Remember Me</label>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-6 col-12 float-sm-left text-center text-sm-right">
                                                <a href="{{ route('password.request') }}" class="card-link text-primary">{{ __('auth.forgot-password-link') }}</a>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-outline-primary btn-block"><i class="ft-unlock"></i> {{ __('auth.btn-login-text') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endsection