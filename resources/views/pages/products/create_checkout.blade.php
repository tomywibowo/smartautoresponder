@extends('layouts.main')

@php
$bgColors = [
    ['name' => 'silver', 'hexa' => '#ededed'],
    ['name' => 'lightgreen', 'hexa' => '#c5fcd9'],
    ['name' => 'grey', 'hexa' => '#c7c7c7'],
    ['name' => 'lightblue', 'hexa' => '#c9e8ff'],
];

$btnColors = [
    ['name' => 'orange', 'hexa' => '#ff8000'],
    ['name' => 'green', 'hexa' => '#099623'],
    ['name' => 'red', 'hexa' => '#ff0000'],
    ['name' => 'blue', 'hexa' => '#0055ff'],
];

$fields = [
    ['optional' => false, 'placeholder' => 'Nama', 'name'   => 'name'],
    ['optional' => false, 'placeholder' => 'No. Handphone/Whatsapp', 'name' => 'number_phone'],
    ['optional' => true, 'placeholder' => 'Masukan Email', 'name'   => 'email'],
    ['optional' => true, 'placeholder' => 'Masukan Catatan', 'name'   => 'note'],
    ['optional' => true, 'placeholder' => 'Alamat Lengkap', 'name'  => 'address'],
    ['optional' => true, 'placeholder' => 'Provinsi', 'name'   => 'province'],
    ['optional' => true, 'placeholder' => 'Kota/Kabupaten', 'name'   => 'city'],
    ['optional' => true, 'placeholder' => 'Kecamatan', 'name'   => 'district'],
];
@endphp

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-clipboard"></i>
        {{ __('pages.products') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.products') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
    @include('pages.products.components.wizzard-navigation')

    <div class="row">
        <div class="col-md-7" id="left-section">
            <div class="card">
                <div class="card-body">
                    <h4 class="mb-2"><strong>Checkout Page Name</strong></h4>
                    <div class="form-group">
                        <input type="text" class="input-sm form-control">
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4><i class="fas fa-columns"></i> <strong>Template</strong></h4>
                    <div class="form-group row">
                        <div class="col">
                            <div class="d-flex">
                                <div>
                                    <p class="mt-1">Right Sidebar</p>
                                    <label for="right-sidebar" onclick="setTemplatePreviewProduct(this)" id="right-sidebar-label" class="sidebar-label active">
                                        <div class="item"></div>
                                        <div class="item"></div>
                                    </label>
                                    <input type="radio" name="sidebar" checked id="right-sidebar" class="d-none" value="right-sidebar">
                                </div>
                                <div class="ml-4">
                                    <p class="mt-1">Left Sidebar</p>
                                    <label for="right-sidebar" onclick="setTemplatePreviewProduct(this)" id="left-sidebar-label" class="sidebar-label reverse">
                                        <div class="item"></div>
                                        <div class="item"></div>
                                    </label>
                                    <input type="radio" name="sidebar" id="left-sidebar" class="d-none" value="left-sidebar">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <p>Choose the background color</p>

                        @foreach ($bgColors as $bgColor)
                        <label for="{{ $bgColor['name'] }}" style="background-color: {{ $bgColor['hexa'] }};" class="bg-color" onclick="setMainBackgroundPreviewProduct(this)">
                            <input type="radio" name="background" id="{{ $bgColor['name'] }}" class="d-none" value="{{ $bgColor['hexa'] }}">
                        </label>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4><i class="fas fa-heading"></i> <strong>Header</strong></h4>
                    <div class="form-group pt-1">
                        <label for="show-tagline">
                            <div class="row icheck_minimal skin pl-1">
                                <input type="checkbox" id="show-tagline" class="show-tagline" value="1">
                                <label for="show-tagline">Do you want to show tagline on top of your cart?</label>
                            </div>
                        </label>
                    </div>
                    <div class="form-group d-none" id="show-tagline-input">
                        <input type="text" class="input-sm form-control" placeholder="Tagline" onkeyup="setTaglineTextPreviewProduct(this.value)">
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4><i class="fas fa-thumbs-up"></i> <strong>Guarantee Seals</strong></h4>
                    <div class="form-group pt-1">
                        <select name="guarantee" onchange="setGuaranteeTextPreviewProduct(this)" id="guarantee-seals" class="input-sm form-control">
                            <option value="MONEY BACK GUARANTEE" selected>MONEY BACK GUARANTEE</option>
                            <option value="100% SATISFACTION SEAL">100% SATISFACTION SEAL</option>
                            <option value="256-BIT ENCRYPTED">256-BIT ENCRYPTED</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body" onclick="syncingRequestedFieldsToPreviewTheme()">
                    <h4 class="mb-1"><i class="fas fa-bars"></i> <strong>Requested Fields</strong></h4>

                    @foreach ($fields as $index => $field)
                    <div class="form-group row">
                        <div class="col-1">
                            <label for="requested_field-{{ $field['name'] }}" class="{{ !$field['optional'] ? 'd-none' : '' }}">
                                <div class="row icheck_minimal skin pl-1">
                                    <input type="checkbox" id="requested_field-{{ $field['name'] }}" name="requested_fields[]" class="show-tagline" value="{{ $field['name'] }}" data-required="1" data-placeholder="{{ $field['placeholder'] }}" data-name="{{ $field['name'] }}">
                                </div>
                            </label>
                        </div>
                        <div class="col-10">
                            <div class="input-group">
                                <input type="text" class="input-sm form-control" id="requested_field-disabled-placeholder-{{ $field['name'] }}" value="{{ $field['placeholder'] }}" disabled>
                                <div class="input-group-append">
                                    <button class="btn btn-sm btn-outline-secondary" onclick="showModalRequestedField('requested_field-{{ $field['name'] }}')">
                                        <i class="fas fa-cog"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4><i class="fas fa-hand-pointer"></i> <strong>Buy Button</strong></h4>
                    <p><strong>Customize your template's buy button below</strong></p>

                    <div class="text-center">
                        <button class="btn mb-2 text-white" id="button-order-checkout-page" style="background-color: #ff8000;">Pesan Sekarang!</button>
                    </div>

                    <div>
                        @foreach ($btnColors as $key => $btnColor)
                        <label for="{{ $btnColor['name'] }}" style="background-color: {{ $btnColor['hexa'] }};" class="bg-color {{ $key == 0 ? 'active' : '' }}" onclick="setButtonOrderBackgroundPreviewProduct(this)">
                            <input type="radio" name="button-order-checkout-page-backgrounds" value="{{ $btnColor['hexa'] }}" id="{{ $btnColor['name'] }}" class="d-none" {{ $key == 0 ? 'checked' : '' }}>
                        </label>
                        @endforeach
                    </div>
                    <p class="mt-2 mb-0"><strong>Enter the text for your button.</strong></p>
                    <p>(Suggestions: 'Buy Now' or 'Complete Now')</p>
                    <div class="form-group">
                        <input type="text" class="input-sm form-control" onkeyup="setTextButtonOrderNow(this)" value="Pesan Sekarang!">
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4><i class="fas fa-play-circle"></i> <strong>Video</strong></h4>
                    <p><strong>Add a video to your sidebar to speak to customers or show off your product.</strong></p>
                    <div class="form-group">
                        <input type="text" class="input-sm form-control" placeholder="Enter youtube video url here">
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4><i class="fas fa-exclamation-circle"></i> <strong>Description</strong></h4>
                    <p><strong>Add product description to explain more about your product</strong></p>
                    <div class="form-group">
                        <textarea name="product_description" id="" cols="30" rows="10" style="height: 6em;" class="form-control" onkeyup="setProductDescription(this)"></textarea>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4><i class="fas fa-ticket-alt"></i> <strong>Coupon Field</strong></h4>
                    <div class="form-group pt-1">
                        <label for="show-tagline">
                            <div class="row icheck_minimal skin pl-1">
                                <input type="checkbox" id="show-tagline" class="show-tagline" value="1">
                                <label for="show-tagline">Do you want to show the coupon field on your cart?</label>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4><i class="fas fa-list-ul"></i> <strong>Bullet Points</strong></h4>
                    <p><strong>Add bullet points to list the benefits or features of your product.</strong></p>
                    
                    <div class="mt-n1" id="bullet-points-wrapper">
                    </div>
                    <div class="form-group mt-1 text-right">
                        <button class="btn btn-sm btn-primary" onclick="addBulletPoint()">Add bullet point</button>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4><i class="fas fa-comments"></i> <strong>Testimonials</strong></h4>
                    <p>Add testimonials to increase social proof to attract more customers.</p>

                    <div class="row">
                        <div class="col-3">
                            <img src="/assets/images/profile-avatar.jpg" alt="profile" class="img-thumbnail border-light">
                        </div>
                        <div class="col-9">
                            <div class="form-group">
                                <input type="text" class="input-sm form-control mb-1" placeholder="Name">
                                <textarea name="" id="" cols="30" rows="10" class="form-control" style="height: 6em;"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt-1 text-right">
                        <button class="btn btn-sm btn-primary">Add Testimonial</button>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <h4><i class="fas fa-code"></i> <strong>Tracking</strong></h4>
                    <p class="mt-2"><strong>Facebook Pixel</strong></p>
                    <p class="mt-1">Pixel IDs</p>
                    <div class="input-group mt-1">
                        <select name="" id="" class="input-sm form-control">
                            <option value="">Select Pixel ID</option>
                        </select>
                    </div>
                    <p class="mt-1 mb-1">Pixel Event</p>
                    <div class="form-group mt-1">
                        <select name="" id="" class="input-sm form-control">
                            <option value="">Select Pixel Event</option>
                            <option value="">AddPaymentInfo</option>
                            <option value="">AddToCart</option>
                            <option value="">AddToWishlist</option>
                            <option value="">CompleteRegistration</option>
                            <option value="">InitiateCheckout</option>
                            <option value="">Lead</option>
                            <option value="">Purchase</option>
                            <option value="">Search</option>
                            <option value="">ViewContent</option>
                        </select>
                    </div>

                    <p class="mt-3"><strong>Google Tag Manager</strong></p>
                    <p class="mt-1">GTM IDs</p>
                    <div class="input-group mt-1">
                        <select name="" id="" class="input-sm form-control">
                            <option value="">Select GTM ID</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="form-group d-flex justify-content-between">
                        <a href="{{ route('products.edit_unique_codes', encrypt($product->id)) }}" class="btn btn-outline-secondary"><i class="fas fa-arrow-left"></i> Back</a>
                        <button class="btn btn-primary">
                            Next <i class="fas fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5" id="right-section">
            <div class="card position-fixed">
                <ul class="nav nav-tabs w-100 row d-flex mx-0" style="height: 50px">
                    <li class="tab-preview-product active col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                        <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-desktop-screen"><i class="la la-desktop"></i> Desktop</a>
                    </li>
                    <li class="tab-preview-product col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                        <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-mobile-screen"><i class="la la-mobile"></i> Mobile</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="preview-desktop-screen" class="tab-pane fade active show">
                        <div id="wrap">
                            <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" id="frame-desktop"></iframe>
                        </div>
                    </div>
                    <div id="preview-mobile-screen" class="tab-pane fade">
                        <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" width="100%" height="700px" id="frame-mobile"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalFormRequestedField" tabindex="-1" role="dialog" aria-labelledby="modalFormRequestedFieldLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered"" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="modal_requested_field-required">Required</label>
                            <select class="form-control" name="modal_requested_field-required" id="modal_requested_field-required">
                                <option value="1">Yes (Required)</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="modal_requested_field-placeholder">Placeholder</label>
                            <input type="text" class="form-control" name="modal_requested_field-placeholder" id="modal_requested_field-placeholder" placeholder="placeholder">
                        </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal_button_requested_field" data-name="" onclick="saveModalRequestedFieldForm(this)">Save changes</button>
                </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/wizard.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/icheck/icheck.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/icheck/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/checkboxes-radios.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">

<style>
    .sidebar-label {
        padding:.3rem;
        width: 8em;
        height: 9em;
        border: 1px solid #c7c7c7;
        display: flex;
        cursor: pointer;
    }

    .sidebar-label.active {
        border: 1.5px solid #118c0a;
    }

    .sidebar-label.active .item {
        background-color: #b5b5b5;
    }

    .sidebar-label .item {
        background-color: #c7c7c7;
        margin: .2rem;
    }

    .sidebar-label > .item:first-child {
        width: 70%;
    }

    .sidebar-label > .item:nth-child(2) {
        width: 30%;
    }

    .sidebar-label.reverse {
        flex-direction: row-reverse;
    }

    .bg-color {
        position: relative;
        height: 2.2em;
        width: 2em;
        border: 1px solid #b5b5b5;
        margin: .3rem;
        cursor: pointer;
    }

    .bg-color.active {
        border: 1px solid #118c0a;
    }

    .bg-color.active::after {
        content: '';
        position: absolute;
        bottom: -1px;
        left: 50%;
        transform: translate(-50%, 0);
        border-left: .5rem solid transparent;
        border-right: .5rem solid transparent;
        border-bottom: .9rem solid white;
    }
</style>

<style>
    .tab-preview-product {
        color: #333;
        background: #FFF;
    }

    .tab-preview-product a {
        color: #333;
    }

    .tab-preview-product.active {
        background: #1BA893;
        color: #FFF;
    }

    .tab-preview-product.active a {
        color: #FFF;
    }
</style>
<style>
    #wrap {
    width: 100%;
    height: 650px;
    padding: 0;
    overflow-x: scroll;
    overflow-y: scroll;
    }

    #frame-desktop {
    width: 1200px;
    height: 1300px;
    border: 0px;
    }

    #frame-desktop {
    zoom: 0.5;
    -moz-transform: scale(0.535);
    -moz-transform-origin: 0 0;
    -o-transform: scale(0.535);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.535);
    -webkit-transform-origin: 0 0;
    }

    @media screen and (-webkit-min-device-pixel-ratio:0) {
    #frame-desktop {
        zoom: 1;
    }
    }
</style>
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/forms/checkbox-radio.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

<script>
$('.sidebar-label').on('click', function() {
    $('.sidebar-label').removeClass('active');
    $(this).addClass('active');
});

$('.bg-color').on('click', function() {
    $('.bg-color').removeClass('active');
    $(this).addClass('active');
});

$('#show-tagline').on('ifChecked', function() {
    $('#show-tagline-input').removeClass('d-none');

    const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
    const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

    const iframePreviewProductElements = [
        iframePreviewProductMobileElement, iframePreviewProductDesktopElement
    ];
    
    iframePreviewProductElements.forEach((iframePreviewProductElement) => {
        let element = iframePreviewProductElement.contentWindow.document.getElementById('template-header-tagline');

        element.classList.contains('d-none') ? element.classList.remove('d-none') : '';
    });
});

$('#show-tagline').on('ifUnchecked', function() {
    $('#show-tagline-input').addClass('d-none');

    const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
    const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

    const iframePreviewProductElements = [
        iframePreviewProductMobileElement, iframePreviewProductDesktopElement
    ];

    iframePreviewProductElements.forEach((iframePreviewProductElement) => {
        let element = iframePreviewProductElement.contentWindow.document.getElementById('template-header-tagline');

        !element.classList.contains('d-none') ? element.classList.add('d-none') : '';
    });
});

</script>

@if(Session::has('update-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Unique Code is edited successfully.',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif

<script>
    function setTemplatePreviewProduct(element) {
        document.getElementsByName('sidebar').forEach((e) => e.hasAttribute('checked') ? e.removeAttribute('checked') : '');
        element.nextElementSibling.setAttribute('checked', true);

        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            iframePreviewProductElement.contentWindow.document.location.href = '{{ url('preview-product?product_id=' . $product->id) }}' + '&product_template=' + element.nextElementSibling.value;
        });
    }

    function setMainBackgroundPreviewProduct(element) {
        document.getElementsByName('background').forEach((e) => e.hasAttribute('checked') ? e.removeAttribute('checked') : '');

        let input = element.children[0];
        input.setAttribute('checked', true);

        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            iframePreviewProductElement.contentWindow.document.getElementById('template-main-background').style.backgroundColor = `${input.value}`;
        });
    }
    
    function setTaglineTextPreviewProduct(value) {
        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            iframePreviewProductElement.contentWindow.document.getElementById('template-header-tagline').innerHTML = value;
        });

    }

    function setGuaranteeTextPreviewProduct(element) {
        let text = element.options[element.selectedIndex].value;

        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            iframePreviewProductElement.contentWindow.document.getElementById('template-guarantee-el').innerHTML = text;
        });
    }

    function syncingRequestedFieldsToPreviewTheme()
    {
        document.getElementsByName('requested_fields[]').forEach((checkbox) => {
            $(checkbox).on('ifChecked', function() {
                checkbox.setAttribute('checked', true);
                generateDisplayingFromToClient();
            });

            $(checkbox).on('ifUnchecked', function() {
                checkbox.removeAttribute('checked');
                generateDisplayingFromToClient();
            });
        })
        generateDisplayingFromToClient();
    }

    function generateDisplayingFromToClient() {
        let checkedCheckbox = [];

        document.getElementsByName('requested_fields[]').forEach((checkbox) => {
            if( checkbox.checked ) {
                checkedCheckbox.push({
                    type: 'text',
                    name: checkbox.value,
                    required: true,
                    placeholder: ''
                });
            }
        });

        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            iframePreviewProductElement.contentWindow.document.querySelectorAll('.form-group.mb-3').forEach((inputClient) => {
                if( !inputClient.classList.contains('required') ) {
                    !inputClient.classList.contains('d-none') ? inputClient.classList.add('d-none') : '';
                }
            });

            checkedCheckbox.forEach((checkedBox) => {
                if(iframePreviewProductElement.contentWindow.document.getElementById(checkedBox.name).parentElement.classList.contains('d-none')) {
                    iframePreviewProductElement.contentWindow.document.getElementById(checkedBox.name).parentElement.classList.remove('d-none')
                };
            })
        });
    }

    function showModalRequestedField(input_requested_field_id) {
        const inputRequestedFieldElement = document.getElementById(input_requested_field_id);
        $('#modalFormRequestedField').modal('show');

        setValueModalRequestedField({
            required: inputRequestedFieldElement.dataset.required,
            placeholder: inputRequestedFieldElement.dataset.placeholder,
            requested_field_name: inputRequestedFieldElement.dataset.name
        });
    }

    function setValueModalRequestedField(inputValueObj) {
        for( key in inputValueObj ) {

            if( key == 'requested_field_name' ) {
                const buttonSaveModalRequestedFieldElement     = document.getElementById('modal_button_requested_field');
                buttonSaveModalRequestedFieldElement.setAttribute('data-name', inputValueObj[key]);
            } else {
                const inputRequestedFieldModalElement   = document.getElementById(`modal_requested_field-${key}`);
                inputRequestedFieldModalElement.value   = inputValueObj[key];
            }
        }
    }

    function saveModalRequestedFieldForm(buttonElement) {
        const name                                          = buttonElement.dataset.name;
        const checkBoxRequestedFieldElement                 = document.getElementById(`requested_field-${name}`);
        const inputRequiredRequestedFieldModalElement       = document.getElementById(`modal_requested_field-required`);
        const inputPlaceholderRequestedFieldModalElement    = document.getElementById(`modal_requested_field-placeholder`);
        const inputRequestedFieldDisabledPlaceholder        = document.getElementById(`requested_field-disabled-placeholder-${name}`)

        checkBoxRequestedFieldElement.dataset.required      = inputRequiredRequestedFieldModalElement.value;
        checkBoxRequestedFieldElement.dataset.placeholder   = inputPlaceholderRequestedFieldModalElement.value;
        inputRequestedFieldDisabledPlaceholder.value        = inputPlaceholderRequestedFieldModalElement.value;

        const iframePreviewProductDesktopElement    = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement     = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            let element = iframePreviewProductElement.contentWindow.document.getElementById(name);
            element.setAttribute('placeholder', checkBoxRequestedFieldElement.dataset.placeholder);
            element.setAttribute('required', checkBoxRequestedFieldElement.dataset.required);
        });

        $('#modalFormRequestedField').modal('hide');
    }

    function setButtonOrderBackgroundPreviewProduct(element) {
        const buttonOrderCheckoutPage = document.getElementById('button-order-checkout-page');
        document.getElementsByName('button-order-checkout-page-backgrounds').forEach((e) => e.hasAttribute('checked') ? e.removeAttribute('checked') : '');

        let input = element.children[0];
        input.setAttribute('checked', true);

        buttonOrderCheckoutPage.style.backgroundColor = input.value;

        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            iframePreviewProductElement.contentWindow.document.getElementById('product_template-button_order_now').style.backgroundColor = `${input.value}`;
        });
    }

    function setTextButtonOrderNow(element) {
        const buttonOrderCheckoutPage   = document.getElementById('button-order-checkout-page');
        let text_value                  = element.value;

        buttonOrderCheckoutPage.innerText = text_value;

        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            iframePreviewProductElement.contentWindow.document.getElementById('product_template-button_order_now').innerText = `${text_value}`;
        });
    }

    function setProductDescription(element) {
        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            iframePreviewProductElement.contentWindow.document.getElementById('paragraph-product-description').innerText = `${element.value}`;
        });
    }

    function addBulletPoint() {
        const wrapperBulletPoints                               = document.getElementById('bullet-points-wrapper');

        wrapperBulletPoints.innerHTML   +=  `<div class="input-group mt-1">
                                                <input type="text" class="input-sm form-control" name="features" value="Fitur ${wrapperBulletPoints.children.length + 1}">
                                                <div class="input-group-append" onclick="deleteBulletThisFeature(this)">
                                                    <button class="btn btn-sm btn-outline-secondary">
                                                        <i class="fas fa-trash"></i> Delete
                                                    </button>
                                                </div>
                                            </div>`;
        
        let inputListFeaturesElement                = document.getElementsByName('features');

        const iframePreviewProductDesktopElement    = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement     = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            iframePreviewProductElement.contentWindow.document.getElementById('product_preview-list_features_wrapper').innerHTML = '';
            inputListFeaturesElement.forEach((input) => {
                iframePreviewProductElement.contentWindow.document.getElementById('product_preview-list_features_wrapper').innerHTML += `<li class="font-medium mb-1"><i class="zmdi zmdi-check font-black text-green-500 mr-1"></i> ${input.value}</li>`;
            });
        });                                     
    }

    function deleteBulletThisFeature(elementParentButtonDelete) {
        const wrapperBulletPointsElement                        = document.getElementById('bullet-points-wrapper');
        const elementClickedBulletPoint                         = elementParentButtonDelete.parentElement;
        let copiedArrayFromWrapperBulletPointsElementChildren   = [];
        let inputListFeaturesElement                            = document.getElementsByName('features');

        for( let i = 0; i < wrapperBulletPointsElement.children.length; i++ ) {
            if(wrapperBulletPointsElement.children[i] != elementClickedBulletPoint) {
                copiedArrayFromWrapperBulletPointsElementChildren.push(wrapperBulletPointsElement.children[i]);
            }
        }

        wrapperBulletPointsElement.innerHTML = '';

        copiedArrayFromWrapperBulletPointsElementChildren.forEach((copiedElementBulletPointElementChildren) => {
            wrapperBulletPointsElement.appendChild(copiedElementBulletPointElementChildren);
        });

        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            iframePreviewProductElement.contentWindow.document.getElementById('product_preview-list_features_wrapper').innerHTML = '';
            inputListFeaturesElement.forEach((input) => {
                iframePreviewProductElement.contentWindow.document.getElementById('product_preview-list_features_wrapper').innerHTML += `<li class="font-medium mb-1"><i class="zmdi zmdi-check font-black text-green-500 mr-1"></i> ${input.value}</li>`;
            });
        });
    }
</script>
@endsection
