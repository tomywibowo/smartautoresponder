@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-clipboard"></i>
        {{ __('pages.products') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.products') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection
 
@section('content')
@include('pages.products.components.wizzard-navigation')
<div class="row">
    <div class="col-md-2 col-lg-2 col-2 d-none d-md-block d-lg-block d-sm-block">
        @include('pages.products.components.section-list')
    </div>

    <div class="col-md-5 col-lg-5 col-sm-10 col-12">
        <div class="card">
            <div class="card-header">
                <h4><i class="la la-link mr-1"></i> Product Create | Unique Code</h4>
                <h5>Setup unique code for each payment</h5>
            </div>
            <form action="{{ route('products.create_unique_codes', encrypt($product->id)) }}" method="post">
                @csrf
                <input type="hidden" name="product_id" value="{{ $product->id }}">
            <div class="card-body">
                <div class="form-grop pl-2">
                    <label for="is-unique-code">
                        <div class="row icheck_minimal skin">
                            <input type="checkbox" id="is-unique-code" name="is_unique_code" value="1">
                            <label for="is-unique-code">Enable Unique Code?</label>
                        </div>
                    </label>
                </div>
                <div id="unique-code-area" class="d-none">
                    <div class="form-group mt-1">
                        <label for="status" class="form-control-label">
                            <span class="text-danger">*</span>
                            Type
                        </label>
                        <div>
                            <input type="hidden" name="type" value="decrease">
                            <div class="btn-group btn-block" role="group" aria-label="Basic example">
                                <button type="button" id="decrease-button" class="btn btn-outline-secondary btn-status active" onclick="
                                        $('.btn-status').removeClass('active');
                                        $(this).addClass('active');
                                        $('input[name=type]').val('decrease')">
                                    Decrease
                                </button>
                                <button type="button" id="increase-button" class="btn btn-outline-secondary btn-status" onclick="
                                        $('.btn-status').removeClass('active');
                                        $(this).addClass('active');
                                        $('input[name=type]').val('increase')">
                                    Increase
                                </button>
                            </div>
                            @error('type')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="range-min">Min</label>
                            <input type="number" id="range-min" class="form-control @error('range.min') is-invalid @enderror" name="range[min]" value="{{ old('range.min') }}">
                            @error('range.min')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <div class="col-md-6">
                            <label for="range-max">Max</label>
                            <input type="number" id="range-max" class="form-control @error('range.max') is-invalid @enderror" name="range[max]" value="{{ old('range.max') }}">
                            @error('range.max')
                                <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-between">
                <button type="button" class="btn btn-outline-secondary" disabled>
                    {{ __('button.previous') }}
                </button>
                <button type="submit" class="btn btn-primary">
                    {{ __('button.next') }}
                </button>
            </div>
            </form>
        </div>
    </div>

    <div class="col-md-5">
        <div class="card">
            <ul class="nav nav-tabs w-100 row d-flex mx-0" style="height: 50px">
                <li class="tab-preview-product active col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-desktop-screen"><i class="la la-desktop"></i> Desktop</a>
                </li>
                <li class="tab-preview-product col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-mobile-screen"><i class="la la-mobile"></i> Mobile</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="preview-desktop-screen" class="tab-pane fade active show">
                    <div id="wrap">
                        <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" id="frame-desktop"></iframe>
                    </div>
                </div>
                <div id="preview-mobile-screen" class="tab-pane fade">
                    <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" width="100%" height="700px" id="frame-mobile"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/wizard.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/icheck/icheck.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/icheck/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/checkboxes-radios.min.css') }}">

<style>
    #wrap {
    width: 100%;
    height: 650px;
    padding: 0;
    overflow-x: scroll;
    overflow-y: scroll;
    }

    #frame-desktop {
    width: 1200px;
    height: 1300px;
    border: 0px;
    }

    #frame-desktop {
    zoom: 0.5;
    -moz-transform: scale(0.535);
    -moz-transform-origin: 0 0;
    -o-transform: scale(0.535);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.535);
    -webkit-transform-origin: 0 0;
    }

    @media screen and (-webkit-min-device-pixel-ratio:0) {
    #frame-desktop {
        zoom: 1;
    }
    }
</style>
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/forms/checkbox-radio.min.js') }}"></script>

<script>
$('#is-unique-code').on('ifChecked', function() {
    $('#unique-code-area').removeClass('d-none');
});

$('#is-unique-code').on('ifUnchecked', function() {
    $('#unique-code-area').addClass('d-none');
});

@if(old('is_unique_code'))
    $('#is-unique-code').iCheck('check');
@endif

@if(old('type'))
    $('.btn-status').removeClass('active');
    $('#{{ old('type') }}-button').addClass('active');
    $('input[type=status]').val('{{ old('type') }}');
@endif
</script>
@endsection