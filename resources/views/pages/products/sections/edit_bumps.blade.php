@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-clipboard"></i>
        {{ __('pages.products') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.products') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
@include('pages.products.components.wizzard-navigation')
<div class="row">
    <div class="col-md-2 col-lg-2 col-2 d-none d-md-block d-lg-block d-sm-block">
        @include('pages.products.components.section-list-edit')
    </div>

    <div class="col-md-5 col-lg-5 col-sm-10 col-12">
        <div class="card">
            <div class="card-header">
                <h4><i class="la la-link mr-1"></i> Product Edit | Bumps</h4>
            </div>
            <form action="{{ route('products.edit_bumps', encrypt($product->id)) }}" method="post">
                <div class="card-body">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                    <textarea name="html" class="d-none"></textarea>
                    <textarea name="subtitle" class="d-none"></textarea>

                    <div class="form-grop pl-2">
                        <label for="is_bump_checkbox">
                            <div class="row icheck_minimal skin">
                                <input type="checkbox" id="is_bump_checkbox" class="is_bump_checkbox" name="is_bump" @if($product->bump) checked @endif>
                                <label for="is_bump_checkbox">Do you want to show a bump offer on this product?</label>
                            </div>
                        </label>
                    </div>

                    <div class="bumps_input pl-2 @if(!($errors->any() || $product->bump)) d-none @endif">
                        <div class="form-group">
                            <label for="name">Bump Product Name</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') ?? $product->bump->name ?? '' }}" onkeyup="setTextBumpProduct(this.value, '#create_bump-product_name')" />
                            @error('name')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="price">Price</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input type="number" name="price" id="price" class="form-control @error('price') is-invalid @enderror" value="{{ old("price") ?? $product->bump->price ?? '' }}" onkeyup="setTextBumpProduct(this.value, '#create_bump-product_price')" />
                            </div>
                            @error('price')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="price">Bump Title</label>
                            <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $product->bump->title ?? '' }}" onkeyup="setTextBumpProduct(this.value, '#create_bump-bump_title')" />
                            @error('title')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="price">Description</label>
                            <textarea name="description" class="form-control @error('description') is-invalid @enderror" onkeyup="setTextBumpProduct(this.value, '#create_bump-bump_description')">{{ old('description') ?? $product->bump->description ?? '' }}</textarea>
                            @error('description')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    </div>

                    <div id="bump_preview" class="row @if(!$product->bump) d-none @endif">
                        <div class="col-12">
                            <hr />
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="btn btn-preview btn-secondary btn-block mb-1">
                                Preview <i class="fa fa-arrow-right"></i>
                            </button>
                        </div>
                        <div id="preview_bumps" class="col-md-8">
                            <div id="bumps_components">
                                <div class="bumps-box">
                                    <div class="bumps-box-title">
                                        <i class="far fa-hand-point-right animated faa-flash" style="color:#FF0000;font-size:22px"></i>
                                        <input type="checkbox" name="bumps" id="bumps" />
                                        <span id="create_bump-bump_title">{{ $product->bump->title ?? 'Write the title bumps' }}</span>
                                    </div>
                                    <div class="bumps-box-content">
                                        <p class="subtitle">
                                            <span id="create_bump-product_name">{{ $product->bump->name ?? 'Bump product name' }}</span>
                                            <span id="create_bump-product_price">{{ $product->bump->price ?? 'price' }}</span>
                                        <p id="create_bump-bump_description">{{ $product->bump->description ?? 'Desription for product bump. Please write in here.' }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-between">
                    <a href="{{ route('products.edit_pricings', encrypt($product->id)) }}" class="btn btn-outline-secondary">
                        {{ __('button.previous') }}
                    </a>
                    <button type="submit" class="btn btn-primary">
                        {{ __('button.save') }}
                    </button>
                    <a href="{{ route('products.edit_shippings', encrypt($product->id)) }}" class="btn btn-outline-secondary">
                        {{ __('button.next') }}
                    </a>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-5">
        <div class="card">
            <ul class="nav nav-tabs w-100 row d-flex mx-0" style="height: 50px">
                <li class="tab-preview-product active col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-desktop-screen"><i class="la la-desktop"></i> Desktop</a>
                </li>
                <li class="tab-preview-product col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-mobile-screen"><i class="la la-mobile"></i> Mobile</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="preview-desktop-screen" class="tab-pane fade active show">
                    <div id="wrap">
                        <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" id="frame-desktop"></iframe>
                    </div>
                </div>
                <div id="preview-mobile-screen" class="tab-pane fade">
                    <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" width="100%" height="700px" id="frame-mobile"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/wizard.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/icheck/icheck.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/icheck/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/checkboxes-radios.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">

<style id="style_bump" type="text/css">
    #bumps_components {
        background: #FFFFC9;
        padding: 10px;
    }

    #bumps_components .bumps-box {
        border: 3px dashed #FF0000;
        padding: 10px;
        min-height: 100px;
    }

    #bumps_components .bumps-box .bumps-box-title {
        background: #FFB33C;
        padding: 8px;
    }

    #bumps_components .bumps-box .bumps-box-title input {
        border: none;
        margin-right: 5px;
    }

    #bumps_components .bumps-box .bumps-box-title span {
        font-weight: bold;
        color: #000;
    }

    #bumps_components .bumps-box .bumps-box-content p.subtitle {
        color: #FF0000;
        font-size: 14px;
        font-weight: bold;
        margin: 8px 0 12px;
    }

    #bumps_components .bumps-box .bumps-box-content p {
        color: #000;
        font-size: 12px;
        margin: 0;
        padding: 0;
    }
</style>

<style>
    #wrap {
    width: 100%;
    height: 650px;
    padding: 0;
    overflow-x: scroll;
    overflow-y: scroll;
    }

    #frame-desktop {
    width: 1200px;
    height: 1300px;
    border: 0px;
    }

    #frame-desktop {
    zoom: 0.5;
    -moz-transform: scale(0.535);
    -moz-transform-origin: 0 0;
    -o-transform: scale(0.535);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.535);
    -webkit-transform-origin: 0 0;
    }

    @media screen and (-webkit-min-device-pixel-ratio:0) {
    #frame-desktop {
        zoom: 1;
    }
    }
</style>
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/inline-edit/jquery.inlineedit.js') }}"></script>
<script src="{{ asset('assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/forms/checkbox-radio.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

<script type="text/javascript">
    $('.inlineEdit').inlineEdit();

    $('.is_bump_checkbox').on('ifChecked', function() {
        $('#is_bump').val(1);
        $('.bumps_input').removeClass('d-none');
        $('#bump_preview').removeClass('d-none');

        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            const element_target = iframePreviewProductElement.contentWindow.document.body.querySelector('#preview_bumps');
            
            if( element_target.classList.contains('d-none') && element_target.classList.contains('is-product-preview') ) {
                element_target.classList.remove('d-none');
            }
        });
    });
    $('.is_bump_checkbox').on('ifUnchecked', function() {
        $('#is_bump').val(0);
        $('.bumps_input').addClass('d-none');
        $('#bump_preview').addClass('d-none')

        $('input[name="name"]').val('');
        $('input[name="price"]').val('');
        $('input[name="title"]').val('');
        $('textarea[name="description"]').val('');

        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            const element_target = iframePreviewProductElement.contentWindow.document.body.querySelector('#preview_bumps');
            
            if( !element_target.classList.contains('d-none') && element_target.classList.contains('is-product-preview') ) {
                element_target.classList.add('d-none');
            }
        });
    });

    $('.btn-preview').on('click', function() {
        var name = $('input[name="name"]').val();
        var price = $('input[name="price"]').val();
        var title = $('input[name="title"]').val();
        var description = $('textarea[name="description"]').val();

        $('.bumps-box-title span').html(title);
        $('.bumps-box-content p').html(description);
        $('.bumps-box-content p.subtitle').html(name + ' + Rp' + price + ',-');
    });

    $('form').on('submit', function(e) {
        if ($('#is_bump_checkbox').is('checked')) {
            e.preventDefault();

            var name = $('input[name="name"]').val();
            var price = $('input[name="price"]').val();
            var title = $('input[name="title"]').val();
            var description = $('textarea[name="description"]').val();

            $('.bumps-box-title span').html(title);
            $('.bumps-box-content p').html(description);
            $('.bumps-box-content p.subtitle').html(name + ' + Rp' + price + ',-');

            var html =
                $('#preview_bumps').html() + ' <style>' +
                $('#style_bump').html() + '</style>';
            alert(html);
            $('textarea[name="html"]').val(html);
        }
    });
</script>

@if(Session::has('update-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Bump is edited successfully.',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif


<script>
    function setTextBumpProduct(value, selector) {
        const elementCreateBumpTitle = document.querySelector(selector);

        if( selector == '#create_bump-product_price' ) {
            elementCreateBumpTitle.innerHTML = value ? rupiahFormat(value, 'Rp. ') : 'Price';
        } else if( selector == '#create_bump-product_name' ) {
            elementCreateBumpTitle.innerHTML = value ? value : 'Bump Product Name';
        } else if( selector == '#create_bump-bump_title' ) {
            elementCreateBumpTitle.innerHTML = value ? value : 'Write the title bumps';
        } else if( selector == '#create_bump-bump_description' ) {
            elementCreateBumpTitle.innerHTML = value ? value : 'Description for product bump. Please write in here.';
        }

        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            const element_target = iframePreviewProductElement.contentWindow.document.body.querySelector(selector);
            
            if( selector == '#create_bump-product_price' ) {
                elementCreateBumpTitle.innerHTML = value ? element_target.innerHTML = rupiahFormat(value, 'Rp. ') : 'Price';
            } else if( selector == '#create_bump-product_name' ) {
                elementCreateBumpTitle.innerHTML = value ? element_target.innerHTML = value : 'Bump Product Name';
            } else if( selector == '#create_bump-bump_title' ) {
                elementCreateBumpTitle.innerHTML = value ? element_target.innerHTML = value : 'Write the title bumps';
            } else if( selector == '#create_bump-bump_description' ) {
                elementCreateBumpTitle.innerHTML = value ? element_target.innerHTML = value : 'Description for product bump. Please write in here.';
            }
        });
    }

    function rupiahFormat(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>
@endsection
