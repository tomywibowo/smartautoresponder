@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-clipboard"></i>
        {{ __('pages.products') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.products') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
@include('pages.products.components.wizzard-navigation')
<div class="row">
    <div class="col-md-2 col-lg-2 col-2 d-none d-md-block d-lg-block d-sm-block">
        @include('pages.products.components.section-list')
    </div>
    <div class="col-md-5 col-lg-5 col-sm-10 col-12">
        <div class="card">
            <div class="card-header">
                <h4><i class="la la-money mr-1"></i> Product Create | Pricing</h4>
            </div>
            <form action="{{ route('products.create_pricings', encrypt($product->id)) }}" method="post" enctype="multipart/form-data">
                <div class="card-body">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                    <input type="hidden" name="is_sale_price" value="0">
                    <input type="hidden" name="is_schedule_sale_price" value="0">

                    <div class="form-group">
                        <label for="regular_price">Regular Price<span class="text-danger">*</span></label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input type="number" name="regular_price" id="regular_price" onkeyup="setTextPreviewPage(this.value, '#product_regular_price')" class="form-control @error('regular_price') is-invalid @enderror">
                        </div>
                        @error('regular_price')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group pl-2">
                        <label for="is_sale_price">
                            <div class="row icheck_minimal skin">
                                <input type="checkbox" id="is_sale_price" class="is_sale_price" onchange="checkIsSalePrice(this)" value="1">
                                <label for="input-5">Sale Price</label>
                            </div>
                        </label>
                        <div class="input-group d-none" id="sale_price_ig">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Rp</span>
                            </div>
                            <input type="number" name="sale_price" id="sale_price" onkeyup="setTextPreviewPage(this.value, '#product_sale_price')" class="form-control @error('sale_price') is-invalid @enderror">
                        </div>
                        @error('sale_price')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group pl-3 d-none" id="schedule">
                        <label for="sale_price">
                            <div class="row icheck_minimal skin">
                                <input type="checkbox" id="is_schedule_sale_price" class="is_schedule_sale_price" value="1">
                                <label for="input-5">Schedule Sale Price</label>
                            </div>
                        </label>
                        <div class="row d-none" id="schedule_input">
                            <div class="col-6">
                                <input type="number" onfocus="(this.type='date')" name="schedule_sale_price[start]" id="schedule_sale_price_start" class="form-control @error('schedule_sale_price') is-invalid @enderror" placeholder="Start Date">
                            </div>
                            <div class="col-6">
                                <input type="number" onfocus="(this.type='date')" name="schedule_sale_price[end]" id="schedule_sale_price_end" class="form-control @error('schedule_sale_price') is-invalid @enderror" placeholder="End Date">
                            </div>
                        </div>
                        @error('sale_price')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-between">
                    <a href="{{ route('products.edit_images', encrypt($product->id)) }}" class="btn btn-outline-secondary" disabled>
                        {{ __('button.previous') }}
                    </a>
                    <button type="submit" class="btn btn-primary">
                        {{ __('button.next') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <ul class="nav nav-tabs w-100 row d-flex mx-0" style="height: 50px">
                <li class="tab-preview-product active col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-desktop-screen"><i class="la la-desktop"></i> Desktop</a>
                </li>
                <li class="tab-preview-product col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-mobile-screen"><i class="la la-mobile"></i> Mobile</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="preview-desktop-screen" class="tab-pane fade active show">
                    <div id="wrap">
                        <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" id="frame-desktop"></iframe>
                    </div>
                </div>
                <div id="preview-mobile-screen" class="tab-pane fade">
                    <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" width="100%" height="700px" id="frame-mobile"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/wizard.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/icheck/icheck.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/icheck/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/checkboxes-radios.min.css') }}">
<style>
    .tab-preview-product {
        color: #333;
        background: #FFF;
    }

    .tab-preview-product a {
        color: #333;
    }

    .tab-preview-product.active {
        background: #1BA893;
        color: #FFF;
    }

    .tab-preview-product.active a {
        color: #FFF;
    }
</style>
<style>
    #wrap {
    width: 100%;
    height: 650px;
    padding: 0;
    overflow-x: scroll;
    overflow-y: scroll;
    }

    #frame-desktop {
    width: 1200px;
    height: 1300px;
    border: 0px;
    }

    #frame-desktop {
    zoom: 0.5;
    -moz-transform: scale(0.535);
    -moz-transform-origin: 0 0;
    -o-transform: scale(0.535);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.535);
    -webkit-transform-origin: 0 0;
    }

    @media screen and (-webkit-min-device-pixel-ratio:0) {
    #frame-desktop {
        zoom: 1;
    }
    }
</style>
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/forms/checkbox-radio.min.js') }}"></script>

<script type="text/javascript">
    $('#is_sale_price').iCheck('uncheck');
    $('.is_sale_price').on('ifChecked', function() {
        $('input[name="sale_price"]').val('');
        $('input[name="is_sale_price"]').val('1');
        $('#sale_price_ig').removeClass('d-none');
        $('#schedule').removeClass('d-none')
    });
    $('.is_sale_price').on('ifUnchecked', function() {
        $('input[name="sale_price"]').val('');
        $('input[name="is_sale_price"]').val('0');
        $('#sale_price_ig').addClass('d-none');
        $('#schedule').addClass('d-none');

        $('input[name="is_schedule_sale_price"]').val('0');
        $('#schedule_sale_price_start').val('');
        $('#schedule_sale_price_end').val('');
        $('#schedule_input').addClass('d-none');
    });
    $('.is_schedule_sale_price').on('ifChecked', function() {
        $('input[name="is_schedule_sale_price"]').val('1');
        $('#schedule_sale_price_start').val('');
        $('#schedule_sale_price_end').val('');
        $('#schedule_input').removeClass('d-none');
    });
    $('.is_schedule_sale_price').on('ifUnchecked', function() {
        $('input[name="is_schedule_sale_price"]').val('0');
        $('#schedule_sale_price_start').val('');
        $('#schedule_sale_price_end').val('');
        $('#schedule_input').addClass('d-none');
    });
</script>

<script>
    function setTextPreviewPage(text, selector)
    {
        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            const elements_target = iframePreviewProductElement.contentWindow.document.body.querySelectorAll(selector);
            elements_target.forEach((element_target) => {
                if( selector == '#product_regular_price' ) {
                    if( !document.getElementById('is_sale_price').checked ) {
                        iframePreviewProductElement.contentWindow.document.body.querySelector('#product_sale_price').innerHTML = '';
                    }

                    element_target.innerHTML = document.getElementById('sale_price').value != '' ? `<strike class="text-red-600 text-xs">${rupiahFormat(text, 'Rp. ')}</strike>`
                                                                                            : rupiahFormat(text, 'Rp. ');
                } else {
                    if( text != '' ) {
                        iframePreviewProductElement.contentWindow.document.body.querySelector('#product_regular_price').innerHTML = `<strike class="text-red-600 text-xs">${rupiahFormat(document.getElementById('regular_price').value, 'Rp. ')}</strike>`;;
                    }

                    element_target.innerHTML = rupiahFormat(text, 'Rp. ');
                }
            });
        });
    }

    document.querySelector('.row.icheck_minimal.skin').addEventListener('click', (event) => {
        if(!event.target.tagName == 'LABEL') {
            if( !document.getElementById('is_sale_price').checked ) {
                const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
                const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

                const iframePreviewProductElements = [
                    iframePreviewProductMobileElement, iframePreviewProductDesktopElement
                ];
                
                iframePreviewProductElements.forEach((iframePreviewProductElement) => {
                    const elements_target = iframePreviewProductElement.contentWindow.document.body.querySelectorAll('#product_sale_price');

                    elements_target.forEach((element_target) => {
                        element_target.innerHTML = '';
                    });
                });

                iframePreviewProductElements.forEach((iframePreviewProductElement) => {
                    const elements_target = iframePreviewProductElement.contentWindow.document.body.querySelectorAll('#product_regular_price');

                    elements_target.forEach((element_target) => {
                        element_target.innerHTML = rupiahFormat(document.getElementById('regular_price').value, 'Rp. ');
                    });
                });
            }
        } else {
            if( document.getElementById('is_sale_price').checked ) {
                const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
                const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

                const iframePreviewProductElements = [
                    iframePreviewProductMobileElement, iframePreviewProductDesktopElement
                ];
                
                iframePreviewProductElements.forEach((iframePreviewProductElement) => {
                    const elements_target = iframePreviewProductElement.contentWindow.document.body.querySelectorAll('#product_sale_price');

                    elements_target.forEach((element_target) => {
                        element_target.innerHTML = '';
                    });
                });

                iframePreviewProductElements.forEach((iframePreviewProductElement) => {
                    const elements_target = iframePreviewProductElement.contentWindow.document.body.querySelectorAll('#product_regular_price');

                    elements_target.forEach((element_target) => {
                        element_target.innerHTML = rupiahFormat(document.getElementById('regular_price').value, 'Rp. ');
                    });
                });
            }
        }
        
    });

    function rupiahFormat(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
</script>
@endsection