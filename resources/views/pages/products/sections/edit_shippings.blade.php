@extends('layouts.main')
@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-clipboard"></i>
        {{ __('pages.products') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.products') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
@include('pages.products.components.wizzard-navigation')
<div class="row">
    <div class="col-md-2 col-lg-2 col-2 d-none d-md-block d-lg-block d-sm-block">
        @include('pages.products.components.section-list-edit')
    </div>

    <div class="col-md-5 col-lg-5 col-sm-10 col-12">
        <div class="card">
            <div class="card-header">
                <h4><i class="la la-link mr-1"></i> Product Edit | Shippings</h4>
            </div>
        <form action="{{ route('products.edit_shippings', encrypt($product->id)) }}" method="post">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="weight">Weight (gr)</label>
                    <input type="number" class="form-control" name="weight" value="{{ old('weight') ?? $product->shipping->weight }}">
                    @error('weight')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group row">
                    <div class="col-md-6 mt-2">
                        <label for="curier-jne">
                            <div class="row icheck_minimal skin">
                                <input type="checkbox" id="curier-jne" class="ml-2 mr-2" name="curiers[]" value="jne">
                                <img src="/assets/images/curiers/jne.png" alt="jne" height="40">
                            </div>
                        </label>
                    </div>
                    <div class="col-md-6 mt-2">
                        <label for="curier-tiki">
                            <div class="row icheck_minimal skin">
                                <input type="checkbox" id="curier-tiki" class="ml-2 mr-2" name="curiers[]" value="tiki">
                                <img src="/assets/images/curiers/tiki.png" alt="tiki" height="40">
                            </div>
                        </label>
                    </div>
                    <div class="col-md-6 mt-2">
                        <label for="curier-pos">
                            <div class="row icheck_minimal skin">
                                <input type="checkbox" id="curier-pos" class="ml-2 mr-2" name="curiers[]" value="pos">
                                <img src="/assets/images/curiers/pos.png" alt="pos" height="40">
                            </div>
                        </label>
                    </div>
                    @error('curiers')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group mt-3">
                    <label for="ship_from">Ship From</label>
                    <select class="form-control" name="ship_from[province]" id="ship_province">
                        <option value="">Choose Province</option>
                    @foreach ($provinces as $province)
                        <option value="{{ $province->province_id }}" @if( (old('ship_from.province') ?? $product->shipping->ship_from->province) == $province->province_id) selected @endif>{{ $province->province }}</option>
                    @endforeach
                    </select>
                    @error('ship_from.province')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group">
                    <select class="form-control" name="ship_from[city]" id="ship_city">
                        <option value="">Choose City</option>
                    </select>
                    @error('ship_from.city')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
            </div>
            <div class="form-group pl-2">
                <input type="hidden" name="is_free_shipping" value="0">
                <input type="checkbox" class="mr-1" id="free-shipping" name="is_free_shipping" value="1">
                <label for="free-shipping">Free Shipping</label>
            @error('is_free_shipping')
                <small class="text-danger">{{ $message }}</small>
            @enderror
            </div>
            <div class="card-footer d-flex justify-content-between">
                <a href="{{ route('products.edit_bumps', encrypt($product->id)) }}" class="btn btn-outline-secondary">
                    {{ __('button.previous') }}
                </a>
                <button type="submit" class="btn btn-primary">
                    {{ __('button.save') }}
                </button>
                <a href="{{ route('products.edit_payments', encrypt($product->id)) }}" class="btn btn-outline-secondary">
                    {{ __('button.next') }}
                </a>
            </div>
        </form>
        </div>
    </div>

    <div class="col-md-5">
        <div class="card">
            <ul class="nav nav-tabs w-100 row d-flex mx-0" style="height: 50px">
                <li class="tab-preview-product active col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-desktop-screen"><i class="la la-desktop"></i> Desktop</a>
                </li>
                <li class="tab-preview-product col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-mobile-screen"><i class="la la-mobile"></i> Mobile</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="preview-desktop-screen" class="tab-pane fade active show">
                    <div id="wrap">
                        <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" id="frame-desktop"></iframe>
                    </div>
                </div>
                <div id="preview-mobile-screen" class="tab-pane fade">
                    <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" width="100%" height="700px" id="frame-mobile"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/wizard.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/icheck/icheck.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/icheck/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">


<style>
    #wrap {
    width: 100%;
    height: 650px;
    padding: 0;
    overflow-x: scroll;
    overflow-y: scroll;
    }

    #frame-desktop {
    width: 1200px;
    height: 1300px;
    border: 0px;
    }

    #frame-desktop {
    zoom: 0.5;
    -moz-transform: scale(0.535);
    -moz-transform-origin: 0 0;
    -o-transform: scale(0.535);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.535);
    -webkit-transform-origin: 0 0;
    }

    @media screen and (-webkit-min-device-pixel-ratio:0) {
    #frame-desktop {
        zoom: 1;
    }
    }
</style>
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/forms/icheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

<script>
@php $curiers = $product->shipping->curiers @endphp
@if(in_array('jne', $curiers))
    $('#curier-jne').iCheck('check');
@endif

@if(in_array('tiki', $curiers))
    $('#curier-tiki').iCheck('check');
@endif

@if(in_array('pos', $curiers))
    $('#curier-pos').iCheck('check');
@endif

@if(old('is_free_shipping') || $product->shipping->is_free_shipping)
    $('#free-shipping').iCheck('check');
@endif

    async function getCity(id) {
        return await fetch(`/api/city/${id}`)
            .then(response => response.json())
            .then(data => {
                if(data.status == 'ok') {
                    return data.cities;
                }

                return [];
            });
    }

    $('#ship_province').change(async function() {
        const id = $(this).val();
        let html = ``;
        const cities = await getCity(id);

        cities.forEach((city) => {
            html += `<option value="${city.city_id}">${city.city_name}</option>`;
        });

        if(cities.length < 1) {
            html = '<option>Choose City</option>';
        }
        
        $('#ship_city').html(html);
    });

@php
    $province = old('ship_from.province') ?? 
        $product->shipping->ship_from->province ?? 0;
    $city = old('ship_from.city') ??
        $product->shipping->ship_from->city ?? 0;
@endphp
@if($province)
    async function fillCity() {
        const id =  {{ $province }};
        const cid = {{ $city }};
        const cities = await getCity(id);
        let html = '';

        cities.forEach((city) => {
            html += `<option value="${city.city_id}" ${(city.city_id == cid) ? 'selected' : ''}>${city.city_name}</option>`;
        });

        if(cities.length < 1) {
            html = '<option>Choose City</option>';
        }
        
        $('#ship_city').html(html);
    }

    fillCity();
@endif
</script>
@if(Session::has('update-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Shipping is edited successfully.',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif
@endsection
