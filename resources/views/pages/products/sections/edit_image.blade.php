@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-clipboard"></i>
        {{ __('pages.products') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.products') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
@include('pages.products.components.wizzard-navigation')
<div class="row">
    <div class="col-md-2 col-lg-2 col-2 d-none d-md-block d-lg-block d-sm-block">
        @include('pages.products.components.section-list-edit')
    </div>

    <div class="col-md-5 col-lg-5 col-sm-10 col-12">
        <div class="card">
            <div class="card-header">
                <h4><i class="la la-image mr-1"></i> Product Edit | Images</h4>
            </div>
            <form action="{{ route('products.edit_images', encrypt($product->id)) }}" method="post" enctype="multipart/form-data">
                <div class="card-body">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="product_id" value="{{ $product->id }}">
                    <input type="hidden" name="dir" value="{{ $product->code }}">
                    <input type="hidden" name="delete_images" id="deleted-images">

                    <div class="form-group custom-input-file mb-0">
                        <label for="image">
                            <span>{{ __('product.fields.images.drop_file') }}</span>
                            <div class="d-none"></div>
                        </label>
                        <input type="file" name="image[]" onchange="setImagePreviewProduct(this, '#product_images_wrapper')" id="image" accept="image/png, image/gif, image/jpeg" multiple="multiple">
                    </div>
                    <div class="form-group" id="removable-images">
                    @foreach ($product->images as $image)
                        <div class="removable-image">
                            <span class="remove-button" data-id="{{ $image->id }}"><i class="la la-close"></i></span>
                            <input type="hidden">
                            <img src="{{ $image->image_url }}">
                        </div>
                    @endforeach
                        <button type="button" id="recover-images" class="btn btn-outline-light m-2"><i class="icon-reload"></i></button>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-between">
                    <a href="{{ route('products.edit', encrypt($product->id)) }}" class="btn btn-outline-secondary">
                        {{ __('button.previous') }}
                    </a>
                    <button type="submit" class="btn btn-primary">
                        {{ __('button.save') }}
                    </button>
                    <a href="{{ route('products.edit_pricings', encrypt($product->id)) }}" class="btn btn-outline-secondary">
                        {{ __('button.next') }}
                    </a>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-5">
        <div class="card">
            <ul class="nav nav-tabs w-100 row d-flex mx-0" style="height: 50px">
                <li class="tab-preview-product active col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-desktop-screen"><i class="la la-desktop"></i> Desktop</a>
                </li>
                <li class="tab-preview-product col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-mobile-screen"><i class="la la-mobile"></i> Mobile</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="preview-desktop-screen" class="tab-pane fade active show">
                    <div id="wrap">
                        <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" id="frame-desktop"></iframe>
                    </div>
                </div>
                <div id="preview-mobile-screen" class="tab-pane fade">
                    <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" width="100%" height="700px" id="frame-mobile"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/wizard.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">

<style type="text/css">
    .custom-input-file input {
        visibility: hidden;
    }

    .custom-input-file label {
        width: 100%;
        min-height: 20vw;
        border: 2px dashed #1BA893;
        border-radius: 20px;
        background-color: #F4F5FA;
        font-size: 18px;
        color: #1BA893;
        padding: 18px;
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .custom-input-file label div img,
    #removable-images .removable-image img {
        width: 100px;
        height: 100px;
        margin: 5px;
        border-radius: 20px;
        padding: 3px;
        border: 1px solid #ddd;
    }

    #removable-images div.removable-image {
        position: relative;
        display: inline;

    }

    .removable-image .remove-button {
        cursor: pointer;
        position: absolute;
        top: -3.2rem;
        right: 0;
        width: 25px;
        height: 25px;
        border-radius: 50%;
        text-align: center;
        line-height: 1.6rem;
        background-color: rgba(245, 245, 245, .8);
        border: 1px solid #ddd;
        display: none;
    }

    .removable-image .remove-button i {
        font-size: .9rem;
        font-weight: 500;
    }

    .removable-image:hover .remove-button {
        display: block;
    }

    #recover-images i {
        font-weight: 700;
    }
</style>
<style>
    .tab-preview-product {
        color: #333;
        background: #FFF;
    }

    .tab-preview-product a {
        color: #333;
    }

    .tab-preview-product.active {
        background: #1BA893;
        color: #FFF;
    }

    .tab-preview-product.active a {
        color: #FFF;
    }
</style>
<style>
    #wrap {
    width: 100%;
    height: 650px;
    padding: 0;
    overflow-x: scroll;
    overflow-y: scroll;
    }

    #frame-desktop {
    width: 1200px;
    height: 1300px;
    border: 0px;
    }

    #frame-desktop {
    zoom: 0.5;
    -moz-transform: scale(0.535);
    -moz-transform-origin: 0 0;
    -o-transform: scale(0.535);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.535);
    -webkit-transform-origin: 0 0;
    }

    @media screen and (-webkit-min-device-pixel-ratio:0) {
    #frame-desktop {
        zoom: 1;
    }
    }
</style>
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
<script type="text/javascript">
    $('.custom-input-file input').on('change', function() {
        imagesPreview(this, '.custom-input-file label div');
    });

    var imagesPreview = function(input, placeTo) {
        if (input.files.length > 5) {
            alert('<?= __('product.fields.images.max_length') ?>');
        } else {
            $('.custom-input-file label').css('align-items', 'flex-start');
            $('.custom-input-file label').css('justify-content', 'flex-start');
            $('.custom-input-file label span').addClass('d-none');
            $('.custom-input-file label div').removeClass('d-none').html('');

            for (i = 0; i < input.files.length; i++) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $($.parseHTML('<img>')).attr('src', e.target.result).appendTo(placeTo);
                };
                reader.readAsDataURL(input.files[i]);
            }
        }
    }

    let deletedImages = [];

    function deleteImage() {
        const encoded = JSON.stringify(deletedImages);
        console.log(encoded);
        $('#deleted-images').val(encoded);
    }

    $('#removable-images').on('click', '.removable-image .remove-button', function() {
        deletedImages.push($(this).data('id'));
        deleteImage();
        $(this).parent().addClass('d-none');
    });

    $('#recover-images').click(function() {
        deletedImages = [];
        deleteImage();
        $('.removable-image').removeClass('d-none');
    });

    
</script>
@if(Session::has('update-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Image is edited successfully.',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif

<script>
    function setImagePreviewProduct(input, selector)
    {
        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');
        let imagesUrl = [];

        for( let i = 0; i < input.files.length; i++ ) {
            let file = input.files[i];

            imagesUrl.push(URL.createObjectURL(file));
        }

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            const element_target = iframePreviewProductElement.contentWindow.document.body.querySelector(selector);
            element_target.innerHTML = '';

            imagesUrl.forEach((imageUrl, index) => {
                if( index == 0 ) {
                    element_target.innerHTML += `<div class="carousel-cell w-100">
                                                        <img src="${imageUrl}" alt="" class="img-thumbnail w-100 img-fluid">
                                                    </div>`;
                }
            });
        });
    }
</script>
@endsection
