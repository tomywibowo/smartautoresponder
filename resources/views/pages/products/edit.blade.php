@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-clipboard"></i>
        {{ __('pages.products') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.products') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
@include('pages.products.components.wizzard-navigation')
<div class="row">
    <div class="col-md-2 col-lg-2 col-2 d-none d-md-block d-lg-block d-sm-block">
        @include('pages.products.components.section-list-edit')
    </div>
    <div class="col-md-5 col-lg-5 col-sm-10 col-12">
        <div class="card">
            <form action="{{ route('products.update', encrypt($product->id)) }}" method="post">
                @csrf
                @method('PUT')
                <div class="card-header">
                    <h4><i class="la la-cubes mr-1"></i> Product Edit</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-control-label">
                            <span class="text-danger">*</span>
                            {{ __('product.field_product_name') }}
                            <small class="text-muted">{{ __('product.field_product_name_note') }}</small>
                        </label>
                        <input type="text" onKeyUp="setTextPreviewPage(this.value, '#product_name')" name="name" value="{{ old('name') ?? $product->name }}" class="form-control @error('name') is-invalid @enderror">

                        @error('name')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="url" class="form-control-label">
                            <span class="text-danger">*</span>
                            {{ __('product.field_checkout_page_url') }}
                        </label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="la la-link"></i></span>
                            </div>
                            <input type="text" name="url" value="{{ old('url') ?? $product->url }}" class="form-control @error('url') is-invalid @enderror">
                        </div>
                        <small>
                            <b>{{ __('product.field_checkout_page_url') }} :</b>
                            <a href="{{ config('app.url') . '/' . (old('url') ?? $product->url) }}" id="generate-url" target="_blank">{{ config('app.url') . '/' . (old('url') ?? $product->url) }}</a>
                        </small>

                        <br />

                        @error('url')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group row">
                        <label for="code" class="form-control-label col-12">
                            <span class="text-danger">*</span>
                            {{ __('product.field_product_code') }}
                        </label>
                        <div class="col-md-5">
                            <input type="text" name="code" value="{{ old('code') ?? $product->code }}" class="form-control @error('code') is-invalid @enderror">
                        </div>

                        @error('code')
                        <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="status" class="form-control-label">
                            <span class="text-danger">*</span>
                            {{ __('product.field_product_status') }}
                        </label>
                        <div>
                            <input type="hidden" name="status" value="live">
                            <div class="btn-group btn-block" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-outline-secondary btn-status @if($product->status == 'disabled') active @endif" onclick="
                                        $('.btn-status').removeClass('active');
                                        $(this).addClass('active');
                                        $('input[name=status]').val('disabled')">
                                    {{ __('product.field_product_status_opt1') }}
                                </button>
                                <button type="button" class="btn btn-outline-secondary btn-status @if($product->status == 'test-mode') active @endif" onclick="
                                        $('.btn-status').removeClass('active');
                                        $(this).addClass('active');
                                        $('input[name=status]').val('test-mode')">
                                    {{ __('product.field_product_status_opt2') }}
                                </button>
                                <button type="button" class="btn btn-outline-secondary btn-status @if($product->status == 'live') active @endif" onclick="
                                        $('.btn-status').removeClass('active');
                                        $(this).addClass('active');
                                        $('input[name=status]').val('live')">
                                    {{ __('product.field_product_status_opt3') }}
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="status" class="form-control-label">
                            {{ __('product.field_product_description') }}
                        </label>
                        <textarea name="description" class="form-control" rows="3">{{ old('description') ?? $product->description }}</textarea>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-between">
                    <button type="button" class="btn btn-outline-secondary" disabled>
                        {{ __('button.previous') }}
                    </button>
                    <button type="submit" class="btn btn-primary">
                        {{ __('button.save') }}
                    </button>
                    <a href="{{ route('products.edit_images', encrypt($product->id)) }}" class="btn btn-outline-secondary">
                        {{ __('button.next') }}
                    </a>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-5">
        <div class="card">
            <ul class="nav nav-tabs w-100 row d-flex mx-0" style="height: 50px">
                <li class="tab-preview-product active col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-desktop-screen"><i class="la la-desktop"></i> Desktop</a>
                </li>
                <li class="tab-preview-product col d-flex align-items-stretch justify-content-center" style="height: 100%;">
                    <a class="w-100 d-flex align-items-center justify-content-center" data-toggle="tab" href="#preview-mobile-screen"><i class="la la-mobile"></i> Mobile</a>
                </li>
            </ul>
            <div class="tab-content">
                <div id="preview-desktop-screen" class="tab-pane fade active show">
                    <div id="wrap">
                        <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" id="frame-desktop"></iframe>
                    </div>
                </div>
                <div id="preview-mobile-screen" class="tab-pane fade">
                    <iframe src="{{ url('preview-product?product_id=' . $product->id) }}" width="100%" height="700px" id="frame-mobile"></iframe>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/plugins/forms/wizard.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">
<style>
    .tab-preview-product {
        color: #333;
        background: #FFF;
    }

    .tab-preview-product a {
        color: #333;
    }

    .tab-preview-product.active {
        background: #1BA893;
        color: #FFF;
    }

    .tab-preview-product.active a {
        color: #FFF;
    }
</style>
<style>
    #wrap {
    width: 100%;
    height: 650px;
    padding: 0;
    overflow-x: scroll;
    overflow-y: scroll;
    }

    #frame-desktop {
    width: 1200px;
    height: 1300px;
    border: 0px;
    }

    #frame-desktop {
    zoom: 0.5;
    -moz-transform: scale(0.535);
    -moz-transform-origin: 0 0;
    -o-transform: scale(0.535);
    -o-transform-origin: 0 0;
    -webkit-transform: scale(0.535);
    -webkit-transform-origin: 0 0;
    }

    @media screen and (-webkit-min-device-pixel-ratio:0) {
    #frame-desktop {
        zoom: 1;
    }
    }
</style>
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
<script type="text/javascript">
    $('input[name="name"]').on('change', function() {
        var name = $(this).val();
        name = name.toLowerCase().replace(" ", "-");
        $('input[name="url"]').val(name);
        $('#generate-url').html('<?= env('APP_URL') ?>/' + name);
        $('#generate-url').attr('href', '<?= env('APP_URL') ?>/' + name);
    });

    $('input[name="url"]').on('change', function() {
        var url = $(this).val();
        url = url.toLowerCase().replace(" ", "-");
        $('input[name="url"]').val(url);
        $('#generate-url').html('<?= env('APP_URL') ?>/' + url);
        $('#generate-url').attr('href', '<?= env('APP_URL') ?>/' + url);
    });
</script>

@if(Session::has('update-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Product is edited successfully.',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif

<script>
    function setTextPreviewPage(text, selector)
    {
        const iframePreviewProductDesktopElement = document.getElementById('frame-desktop');
        const iframePreviewProductMobileElement = document.getElementById('frame-mobile');

        const iframePreviewProductElements = [
            iframePreviewProductMobileElement, iframePreviewProductDesktopElement
        ];
        
        iframePreviewProductElements.forEach((iframePreviewProductElement) => {
            const elements_target = iframePreviewProductElement.contentWindow.document.body.querySelectorAll(selector);
            elements_target.forEach((element_target) => {
                element_target.innerHTML = text;
            });
        });
    }
</script>
@endsection
