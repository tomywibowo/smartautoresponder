<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

$route = Route::currentRouteName();
?>

<div class="card">
    <div class="card-body">
        <div class="list-group list-group-flush">

            <a href="{{ 
                $route == 'products.create' ? 
                    route('products.create') : 
                    route('products.edit', encrypt($product->id)) 
                }}" class="list-group-item {{ $route == 'products.create' ? 'active' : '' }}" id="inbox-menu">

                <span class="d-none d-flex flex-column align-items-center justify-content-center">
                    <i class="la la-cubes"></i>Product
                </span>
            </a>

            @php
            $images = 0;
            if (isset($product->id)) {
            $images = DB::table('product_images')->where('product_id', '=', $product->id)->count();
            }
            @endphp
            <a href="{{ 
                $images > 0 ? 
                    route('products.edit_images', encrypt($product->id)) :
                    'javascript::void(0)'                    
                }}" class="list-group-item
                    {{ $images > 0 ? '' : 'disabled' }}
                    {{ $route == 'products.create_images' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-flex flex-column align-items-center justify-content-center">
                    <i class="la la-image"></i>Images
                </span>
            </a>

            @php
            $pricing = 0;
            if (isset($product->id)) {
            $pricing = DB::table('product_pricings')->where('product_id', '=', $product->id)->count();
            }
            @endphp
            <a href="{{ 
                $pricing > 0 ? 
                    route('products.edit_pricings', encrypt($product->id)) :
                    'javascript::void(0)' 
                }}" class="list-group-item
                    {{ $pricing > 0 ? '' : 'disabled' }}
                    {{ $route == 'products.create_pricings' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-flex flex-column align-items-center justify-content-center">
                    <i class="la la-money"></i>Pricing
                </span>
            </a>

            @php
            $bump = 0;
            if (isset($product->id)) {
            $bump = DB::table('product_bumps')->where('product_id', '=', $product->id)->count();
            }
            @endphp
            <a href="{{ 
                $bump > 0 ? 
                    route('products.edit_bumps', encrypt($product->id)) :
                    'javascript::void(0)' 
                }}" class="list-group-item
                    {{ $bump > 0 ? '' : 'disabled' }}
                    {{ $route == 'products.create_bumps' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-flex flex-column align-items-center justify-content-center">
                    <i class="la la-link"></i>Bump
                </span>
            </a>

            @php
            $shipping = 0;
            if (isset($product->id)) {
            $shipping = DB::table('product_shippings')->where('product_id', '=', $product->id)->count();
            }
            @endphp
            <a href="{{ 
                $shipping > 0 ? 
                    route('products.edit_shippings', encrypt($product->id)) :
                    'javascript::void(0)' 
                }}" class="list-group-item
                    {{ $shipping > 0 ? '' : 'disabled' }}
                    {{ $route == 'products.create_shippings' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-flex flex-column align-items-center justify-content-center">
                    <i class="la la-truck"></i>Shipping
                </span>
            </a>

            @php
            $payment = 0;
            if (isset($product->id)) {
            $payment = DB::table('product_payments')->where('product_id', '=', $product->id)->count();
            }
            @endphp
            <a href="{{ 
                $payment > 0 ? 
                    route('products.edit_payments', encrypt($product->id)) :
                    'javascript::void(0)' 
                }}" class="list-group-item
                    {{ $payment > 0 ? '' : 'disabled' }}
                    {{ $route == 'products.create_payments' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-flex flex-column align-items-center justify-content-center">
                    <i class="la la-cart-arrow-down"></i>Payment
                </span>
            </a>

            @php
            $product_unique_codes = 0;
            if (isset($product->id)) {
            $product_unique_codes = DB::table('product_unique_codes')->where('product_id', '=', $product->id)->count();
            }
            @endphp
            <a href="{{ 
                $product_unique_codes > 0 ? 
                    route('products.edit_unique_codes', encrypt($product->id)) :
                    'javascript::void(0)' 
                }}" class="list-group-item
                    {{ $product_unique_codes > 0 ? '' : 'disabled' }}
                    {{ $route == 'products.create_unique_codes' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-flex flex-column align-items-center justify-content-center">
                    <i class="la la-code-fork"></i>Unique Code
                </span>
            </a>
        </div>
    </div>
</div>