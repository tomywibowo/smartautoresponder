<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

$route = Route::currentRouteName();
?>

<div class="card">
    <div class="card-body">
        <div class="list-group list-group-flush">

            <a href="{{ 
                    route('products.edit', encrypt($product->id))
                }}" class="list-group-item {{ $route == 'products.edit' ? 'active' : '' }}" id="inbox-menu">

                <span class="d-none d-md-inline d-lg-inline">
                    <i class="la la-cubes mr-1"></i>Product
                </span>
            </a>

            <a href="{{
                    route('products.edit_images', encrypt($product->id))
                }}" class="list-group-item
                    {{ $route == 'products.edit_images' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-md-inline d-lg-inline">
                    <i class="la la-image mr-1"></i>Images
                </span>
            </a>

            <a href="{{ 
                    route('products.edit_pricings', encrypt($product->id))
                }}" class="list-group-item
                    {{ $route == 'products.edit_pricings' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-md-inline d-lg-inline">
                    <i class="la la-money mr-1"></i>Pricing
                </span>
            </a>

            <a href="{{ 
                    route('products.edit_bumps', encrypt($product->id))
                }}" class="list-group-item
                    {{ $route == 'products.edit_bumps' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-md-inline d-lg-inline">
                    <i class="la la-link mr-1"></i>Bump
                </span>
            </a>

            <a href="{{ 
                    route('products.edit_shippings', encrypt($product->id))
                }}" class="list-group-item
                    {{ $route == 'products.edit_shippings' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-md-inline d-lg-inline">
                    <i class="la la-truck mr-1"></i>Shipping
                </span>
            </a>

            <a href="{{ 
                    route('products.edit_payments', encrypt($product->id))
                }}" class="list-group-item
                    {{ $route == 'products.edit_payments' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-md-inline d-lg-inline">
                    <i class="la la-cart-arrow-down mr-1"></i>Payment
                </span>
            </a>

            <a href="{{ 
                    route('products.edit_unique_codes', encrypt($product->id))
                }}" class="list-group-item
                    {{ $route == 'products.edit_unique_codes' ? 'active' : '' }}" id="inbox-menu">
                <span class="d-none d-md-inline d-lg-inline">
                    <i class="la la-code-fork mr-1"></i>Unique Code
                </span>
            </a>
        </div>
    </div>
</div>
