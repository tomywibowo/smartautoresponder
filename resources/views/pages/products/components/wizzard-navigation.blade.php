@php
    $route = Route::currentRouteName();
    $checkoutR = 'products.create_checkout_page';
@endphp

<div class="row d-none d-lg-block d-md-block">
    <div class="col-md-12">
        <div class="card card-body p-0">
            <div class="number-tab-steps wizard-notification wizard clearfix">
                <div class="steps clearfix">
                    <ul role="tablist">
                        <li class="first @if($route != $checkoutR)
                        current
                        @endif">
                            <a><span class="step">1</span> Product</a>
                        </li>
                        <li class="second @if($route == $checkoutR)
                            current
                        @endif">
                            <a><span class="step">2</span> Checkout Page</a>
                        </li>

                        <li class="third disabled">
                            <a><span class="step">3</span> Success Page</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
