@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-group"></i>
        Roles
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Roles</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-header-right text-right col-md-6 col-12">
    @can('users-update')
    <button type="button" class="btn btn-cancel btn-secondary text-white d-none">
        <i class="la la-refresh"></i> {{ __('button.cancel') }}
    </button>
    <button type="button" class="btn btn-edit btn-secondary text-white">
        <i class="la la-edit"></i> {{ __('button.edit') }}
    </button>
    @endcan

    @can('users-delete')
    <button type="button" class="btn btn-destroy btn-danger text-white">
        <i class="la la-trash"></i> {{ __('button.destroy') }}
    </button>
    @endcan

    @can('users-create')
    <a href="{{ route('users.create') }}" class="btn btn-primary text-white">
        <i class="la la-plus-circle"></i> {{ __('button.create') }}
    </a>
    @endcan
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <form action="{{ route('roles.update', encrypt($role->id)) }}" method="post">
                @csrf
                @method('put')

                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('roles.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Name :</label>
                        <div class="col-md-5">
                            <input type="text" name="name" id="name" value="{{ old('name') ? old('name') : $role->name }}" class="form-control @error('name') is-invalid @enderror" disabled />

                            @error('name')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Permission Lists</th>
                                <th class="text-center">Read</th>
                                <th class="text-center">Create</th>
                                <th class="text-center">Update</th>
                                <th class="text-center">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($permissions as $permission)
                            <tr>
                                <td>{{ $permission->fullname }}</td>
                                <td class="text-center">
                                    <input type="checkbox" name="permission[]" @if(in_array($permission->id, $rolePermissions)) checked @endif value="{{ $permission->id }}" disabled>
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" name="permission[]" @if(in_array($permission->create->id, $rolePermissions)) checked @endif value="{{ $permission->create->id }}" disabled>
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" name="permission[]" @if(in_array($permission->update->id, $rolePermissions)) checked @endif value="{{ $permission->update->id }}" disabled>
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" name="permission[]" @if(in_array($permission->delete->id, $rolePermissions)) checked @endif value="{{ $permission->delete->id }}" disabled>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('roles.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<form id="destroy-action" action="{{ route('roles.destroy', encrypt($role->id)) }}" method="post" class="d-none">
    @csrf
    @method('delete')
</form>

@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        'use strict';
    });

    $('.btn-edit').on('click', () => {
        $('.btn-edit').addClass('d-none');
        $('.btn-cancel').removeClass('d-none');
        $('.btn-save').removeAttr('disabled');

        $('input').removeAttr('disabled');
    });

    $('.btn-cancel').on('click', () => {
        $('.btn-edit').removeClass('d-none');
        $('.btn-cancel').addClass('d-none');
        $('.btn-save').attr('disabled', 'disabled');

        $('input').attr('disabled', '');
    });

    $('.btn-destroy').on('click', () => {
        Swal.fire({
            title: 'Are you sure?',
            text: "Are you going to destroy this data?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-secondary',
            confirmButtonText: 'Yes, destroy it!',

        }).then((result) => {
            if (result.isConfirmed) {
                $('#destroy-action').submit();
            }
        })
    });
</script>

@if(Session::has('store-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Action create new user is successfully.',
            confirmButtonClass: 'btn btn-primary',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif

@if(Session::has('update-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Action update user is successfully.',
            confirmButtonClass: 'btn btn-primary',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif
@endsection