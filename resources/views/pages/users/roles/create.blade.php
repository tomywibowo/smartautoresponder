@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-group"></i>
        Roles
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Roles</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <form action="{{ route('roles.store') }}" method="post">
                @csrf

                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('roles.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Name :</label>
                        <div class="col-md-5">
                            <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" />

                            @error('name')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Permission Lists</th>
                                <th class="text-center">Read</th>
                                <th class="text-center">Create</th>
                                <th class="text-center">Update</th>
                                <th class="text-center">Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($permissions as $permission)
                            <tr>
                                <td>{{ $permission->fullname }}</td>
                                <td class="text-center">
                                    <input type="checkbox" name="permission[]" @if(old('permission') && in_array($permission->id, old('permission'))) checked @endif value="{{ $permission->id }}">
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" name="permission[]" @if(old('permission') && in_array($permission->create->id, old('permission'))) checked @endif value="{{ $permission->create->id }}">
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" name="permission[]" @if(old('permission') && in_array($permission->update->id, old('permission'))) checked @endif value="{{ $permission->update->id }}">
                                </td>
                                <td class="text-center">
                                    <input type="checkbox" name="permission[]" @if(old('permission') && in_array($permission->delete->id, old('permission'))) checked @endif value="{{ $permission->delete->id }}">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('roles.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('stylesheet')
@endsection

@section('javascript')
@endsection