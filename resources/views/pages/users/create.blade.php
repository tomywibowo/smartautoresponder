@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-user"></i>
        Users
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Users</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <form action="{{ route('users.store') }}" method="post">
                @csrf
                <input type="hidden" name="email_verified_at" value="{{ date('Y-m-d H:i:s') }}">

                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('users.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Name :</label>
                        <div class="col-md-5">
                            <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" />

                            @error('name')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">E-Mail Address :</label>
                        <div class="col-md-5">
                            <input type="email" name="email" id="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" />

                            @error('email')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Username :</label>
                        <div class="col-md-5">
                            <input type="text" name="username" id="username" value="{{ old('username') }}" class="form-control @error('username') is-invalid @enderror" />

                            @error('username')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Password :</label>
                        <div class="col-md-5">
                            <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" />

                            @error('password')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Password Confirmation :</label>
                        <div class="col-md-5">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" />

                            @error('password_confirmation')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Roles :</label>
                        <div class="col-md-5">
                            <select name="roles" data-placeholder="" id="roles" class="form-control select2 @error('roles') is-invalid @enderror">
                                <option value=""></option>
                                @foreach($roles as $role)
                                <option value="{{ $role }}" <?= old('roles') == $role ? 'selected' : '' ?>>{{$role}}</option>
                                @endforeach
                            </select>

                            @error('roles')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('users.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/selects/select2.min.css') }}">
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        'use strict';

        $(".select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });
    });
</script>
@endsection