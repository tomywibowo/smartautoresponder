@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-shield"></i>
        {{ __('pages.permissions') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.permissions') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-body">
            <table class="table datatable" width="100%"></table>
        </div>
    </div>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>

<script type="text/javascript">
    var selected = [];

    $(document).ready(function() {
        $('.datatable').DataTable({
            width: '100%',
            processing: true,
            ajax: {
                url: '/permissions?type=json',
                dataSrc: (data) => {
                    return data;
                }
            },
            order: [0, 'asc'],
            columns: [{
                data: 'fullname',
                title: 'Name',
                orderable: true,
            }, {
                defaultContent: '',
                title: 'Read',
                orderable: false,
                className: "text-center",
                width: "12%",
                render: (row, type, data) => {
                    return `<i class="la la-check"></i>`;
                }
            }, {
                data: 'create',
                title: 'Create',
                orderable: false,
                className: "text-center",
                width: "12%",
                render: (row, type, data) => {
                    return `<i class="la la-check"></i>`;
                }
            }, {
                data: 'update',
                title: 'Update',
                orderable: false,
                className: "text-center",
                width: "12%",
                render: (row, type, data) => {
                    return `<i class="la la-check"></i>`;
                }
            }, {
                data: 'delete',
                title: 'Delete',
                orderable: false,
                className: "text-center",
                width: "12%",
                render: (row, type, data) => {
                    return `<i class="la la-check"></i>`;
                }
            }]
        });
    });
</script>
@endsection