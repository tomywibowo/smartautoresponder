@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-user"></i>
        Users
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('users.index') }}">Users</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-header-right text-right col-md-6 col-12">
    @can('users-update')
    <button type="button" class="btn btn-cancel btn-secondary text-white d-none">
        <i class="la la-refresh"></i> {{ __('button.cancel') }}
    </button>
    <button type="button" class="btn btn-edit btn-secondary text-white">
        <i class="la la-edit"></i> {{ __('button.edit') }}
    </button>
    @endcan

    @can('users-delete')
    <button type="button" class="btn btn-destroy btn-danger text-white">
        <i class="la la-trash"></i> {{ __('button.destroy') }}
    </button>
    @endcan

    @can('users-create')
    <a href="{{ route('users.create') }}" class="btn btn-primary text-white">
        <i class="la la-plus-circle"></i> {{ __('button.create') }}
    </a>
    @endcan
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <form action="{{ route('users.update', encrypt($user->id)) }}" method="post">
                @csrf
                @method('put')

                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('users.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" disabled class="btn btn-save btn-primary">
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Name :</label>
                        <div class="col-md-5">
                            <input type="text" name="name" id="name" value="{{ old('name') ? old('name') : $user->name }}" class="form-control @error('name') is-invalid @enderror" readonly />

                            @error('name')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">E-Mail Address :</label>
                        <div class="col-md-5">
                            <input type="email" name="email" id="email" value="{{ old('email') ? old('email') : $user->email }}" class="form-control @error('email') is-invalid @enderror" readonly />

                            @error('email')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Username :</label>
                        <div class="col-md-5">
                            <input type="text" name="username" id="username" value="{{ old('username') ? old('username') : $user->username }}" class="form-control @error('username') is-invalid @enderror" readonly />

                            @error('username')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Password :</label>
                        <div class="col-md-5">
                            <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" readonly />

                            @error('password')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Password Confirmation :</label>
                        <div class="col-md-5">
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" readonly />

                            @error('password_confirmation')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-2 text-md-right" style="padding: .75rem 0;">Roles :</label>
                        <div class="col-md-5">
                            <input type="text" value="{{ $userRole[0] }}" class="form-control roles-values" readonly>
                            <div class="d-none roles-selection">
                                <select name="roles" data-placeholder="" id="roles" class="form-control select2 @error('roles') is-invalid @enderror">
                                    <option value=""></option>
                                    @foreach($roles as $role)
                                    <option value="{{ $role }}" <?= old('roles') == $role ? 'selected' : '' ?> <?= $userRole[0] == $role ? 'selected' : '' ?>>{{$role}}</option>
                                    @endforeach
                                </select>
                            </div>

                            @error('roles')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('users.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" disabled class="btn btn-save btn-primary">
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<form id="destroy-action" action="{{ route('users.destroy', encrypt($user->id)) }}" method="post" class="d-none">
    @csrf
    @method('delete')
</form>

@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/forms/selects/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        'use strict';

        $(".select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });
    });

    $('.btn-edit').on('click', () => {
        $('.btn-edit').addClass('d-none');
        $('.btn-cancel').removeClass('d-none');
        $('.btn-save').removeAttr('disabled');

        $('input').removeAttr('readonly');

        $('.roles-values').addClass('d-none');
        $('.roles-selection').removeClass('d-none');
    });

    $('.btn-cancel').on('click', () => {
        $('.btn-edit').removeClass('d-none');
        $('.btn-cancel').addClass('d-none');
        $('.btn-save').attr('disabled', 'disabled');

        $('input').attr('readonly', 'readonly');

        $('.roles-values').removeClass('d-none');
        $('.roles-selection').addClass('d-none');
    });

    $('.btn-destroy').on('click', () => {
        Swal.fire({
            title: 'Are you sure?',
            text: "Are you going to destroy this data?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-secondary',
            confirmButtonText: 'Yes, destroy it!',

        }).then((result) => {
            if (result.isConfirmed) {
                $('#destroy-action').submit();
            }
        })
    });
</script>

@if(Session::has('store-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Action create new user is successfully.',
            confirmButtonClass: 'btn btn-primary',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif

@if(Session::has('update-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Action update user is successfully.',
            confirmButtonClass: 'btn btn-primary',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif
@endsection