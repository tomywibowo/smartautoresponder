@extends('layouts.main')

@php
    $oldDests = old('destinations') ?? [];
@endphp

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-whatsapp"></i>
        {{ __('pages.whatsapps') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.whatsapps') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6 col-sm-10 col-12">
        <div class="card">
            <form action="{{ route('broadcasts.store') }}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="sender">Sender</label>
                        <select name="sender" id="sender" data-placeholder="{{ count($senders) ? 'Choose Sender' : 'Please integrate whatsapp first.' }}" class="form-control @error('sender') is-invalid @enderror" style="width: 100%;">
                            <option value=""></option>
                            @foreach ($senders as $sender)
                            <option value="{{ $sender->phone_number }}">{{ $sender->phone_number }}</option>
                            @endforeach
                        </select>
                        @error('sender')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="destinations">Destination WA Number</label>
                        <div id="destination-selection">
                            <select name="destinations[]" class="form-control" id="destinations" placeholder="No contact selected" autocomplete="off" multiple style="width: 100%;">
                                @foreach($dests as $dest)
                                <option value="{{ $dest->phone }}" @if(in_array($dest->phone, $oldDests)) selected @endif)>{{ $dest->name }} - {{ $dest->phone }}</option>
                                @endforeach
                            </select>
                            <!-- <div id="contact-list" class="mt-2"></div> -->
                        </div>
                        <div class="form-check mb-2">
                            <input class="form-check-input" type="checkbox" name="dests_all" id="dests-all">
                            <label class="form-check-label" for="dests-all">
                                Send to all contacts
                            </label>
                        </div>
                        @error('destinations')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        @foreach($errors->get('destinations.*') as $msgs)
                            @foreach($msgs as $msg)
                                <small class="text-danger">{{ $msg }}</small>
                            @endforeach
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" class="form-control @error('message') is-invalid @enderror">{{ old('message') }}</textarea>
                        @error('message')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                        <div class="text-left">
                            <p class="mt-2 ml-1 mb-0">Tips:</p>
                            <ul>
                                <li>You can use dynamic string for generate random string. Rule: (Hai | Hello | Halo)</li>
                                <li>You can call receiver name using {name}</li>
                                <li>Write receiver email with {email}</li>
                                <li>Also, write receiver phone using {phone}</li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="date">Date</label>
                        <input type="date" name="date" id="date" class="form-control @error('date') is-invalid @enderror">
                        @error('date')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="time">Time</label>
                        <input type="time" name="time" id="time" class="form-control @error('time') is-invalid @enderror">
                        @error('time')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="repetition">Repetition</label>
                        <select name="period" id="repetition" class="form-control @error('period') is-invalid @enderror">
                            <option value="">Disable</option>
                            <option value="hourly">Hourly</option>
                            <option value="daily">Daily</option>
                            <option value="weekly">Weekly</option>
                            <option value="monthly">Monthly</option>
                            <option value="annual">Annual</option>
                        </select>
                        @error('period')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-primary btn-block">
                            Send Now
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@tarekraafat/autocomplete.js@10.2.6/dist/css/autoComplete.02.min.css">
<style>
    .select2-container--classic .select2-selection--multiple .select2-selection__choice,
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        border: 1px solid #aaa !important;
        background: #e4e4e4 !important;
        color: #6B6F82 !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice__display {
        padding-left: 15px;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove,
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove:hover,
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove:focus {
        margin-left: 0;
        background-color: transparent;
        color: #6B6F82;
    }

    .autoComplete_wrapper>input {
        border: 1px solid #CACFE7;
        border-radius: .25rem;
        display: block;
        color: #3B4781;
        width: 100%;
    }

    .autoComplete_wrapper>ul>li {
        font-size: 1rem;
    }

    #contact-list {
        border: 1px dashed #CACFE7;
        border-radius: .25rem;
        padding-top: .50rem;
        min-height: 3.5rem;
    }

    .contact-list-item i {
        font-size: 1rem !important;
        cursor: pointer;
    }

    .contact-list-item i:hover {
        font-weight: bold;
    }
</style>
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@tarekraafat/autocomplete.js@10.2.6/dist/autoComplete.min.js"></script>

<script type="text/javascript">
    $('select[name="sender"]').select2({
        minimumResultsForSearch: -1
    });
    $('select[name="destinations[]"]').select2({
        allowClear: true,
        placeholder: "Choose one or more destination to broadcast"
    });
</script>

<script>
    $('#dests-all').change(function() {
        if (!$(this).is(':checked')) {
            $('#destination-selection').removeClass('d-none');
            // $('#contact-list').removeClass('d-none');
        } else {
            $('#destination-selection').addClass('d-none')
            // $('#contact-list').addClass('d-none');
        }
    });
</script>

@if($sess = Session::has('has-sent'))
<script type="text/javascript">
    @php $status = Session::get('has-sent') @endphp
    $(document).ready(function() {
        Swal.fire({
            icon: '<?= ($status) ? 'success' : 'error' ?>',
            title: '<?= ($status) ? 'Success' : 'Failed' ?>',
            text: '<?= Session::get('message') ?>',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 3000,
            timerProgressBar: true,
        });
    });
</script>
@endif

@if($sess = Session::has('error-send'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'error',
            title: 'Failed',
            text: 'No contact selected or contact is empty',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif
@endsection
