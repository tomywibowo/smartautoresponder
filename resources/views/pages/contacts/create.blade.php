@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-phone-square"></i>
        {{ __('pages.contacts') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.contacts') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h4>Create Contact</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('contacts.store') }}" method="post">
                    @csrf
                    @include('pages.contacts.components.form')
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

@if(Session::has('create-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Contact saved successfully.',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif

@endsection
