<div class="form-group">
    <label for="name"><span class="text-danger">*</span>Name</label>
    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') ?? $contact->name ?? '' }}">
    @error('name')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>
<div class="form-group">
    <label for="phone"><span class="text-danger">*</span>Phone</label>
    <input type="number" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ old('phone') ?? $contact->phone ?? '' }}">
    @error('phone')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>
<div class="form-group">
    <label for="email"><span class="text-danger">*</span>Email</label>
    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') ?? $contact->email ?? '' }}">
    @error('email')
    <div class="invalid-feedback">
        {{ $message }}
    </div>
    @enderror
</div>
<div class="form-group d-flex justify-content-between">
    <a href="{{ route('contacts.index') }}" class="btn btn-outline-secondary">Back</a>
    <button type="submit" class="btn btn-primary">Save</button>
</div>
