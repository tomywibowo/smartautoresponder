@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-google"></i>
        {{ __('pages.googlecontacts') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.googlecontacts') }}</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<div id="auth-code" data-code=""></div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h4>Create Google Contact Integration</h4>
            </div>
            <div class="card-body">
                <form method="post" id="gcontact-form">
                    @csrf
                    @include('pages.integrate.googlecontact.components.form')
                    <input type="hidden" name="access_token" id="access_token">
                    <input type="hidden" name="created" id="created">
                    <input type="hidden" name="expires_in" id="expires_in">
                    <input type="hidden" name="refresh_token" id="refresh_token">
                    <input type="hidden" name="scope" id="scope">
                    <input type="hidden" name="token_type" id="token_type">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
<script>
    $.oauthpopup = function(options) {
        options.windowName = options.windowName ||  'ConnectWithOAuth';
        options.windowOptions = options.windowOptions || 'location=0,status=0,width=800,height=400';
        options.callback = options.callback || function(){ window.location.reload(); };
        var that = this;
        that._oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
        that._oauthInterval = window.setInterval(function(){
            if (that._oauthWindow.closed) {
                window.clearInterval(that._oauthInterval);
                options.callback();
            }
        }, 1000);
    };

    function fetchAccTokenWithAuthCode(code) {
        if(code.length < 1) {
            return Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Cannot get auth code',
                showConfirmButton: false,
                buttonsStyling: false,
                timer: 2000,
                timerProgressBar: true,
            });
        }

        const authData = {};
        $('#gcontact-form').serializeArray().map(function(data) {
            authData[data.name] = data.value;
        });

        $.ajax({
            url: '{{ route('google-contact.fetchCode') }}',
            method: 'POST',
            dataType: 'json',
            data: {
                _token: authData._token,
                client: authData,
                accessCode: code
            },
            success: function(res) {
                if(res.status == 'success') {
                    const form = $('#gcontact-form');

                    $.each(res.data, function(name, value) {
                        $(`#${name}`).val(value);
                    });

                    $('#btn-save-gauth').text('Save');
                    form.attr('action', '{{ route('google-contact.store') }}');
                    form.off('submit');
                    form.submit();
                }
            },
            error: function(res, status, err) {
                if(res.status == 422) {
                    const error = res.responseJSON;

                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: error.message,
                        showConfirmButton: false,
                        buttonsStyling: false,
                        timer: 2000,
                        timerProgressBar: true,
                    });
                }
            }
        });
    }

    $('#gcontact-form').submit(function(e) {
        e.preventDefault();
        let form = $(this);

        $('.form-control').removeClass('is-invalid');
        $('.invalid-feedback').html('');

        $.ajax({
            url: '{{ route('google-contact.auth') }}',
            method: 'post',
            data: form.serialize(),
            success: function(e) {
                $.oauthpopup({
                    path: e,
                    callback: function() {
                        const authCode = document.getElementById('auth-code');
                        fetchAccTokenWithAuthCode(authCode.dataset.code);
                    }
                });
            },
            error: function(res, status, err) {
                if(res.status == 400) {
                    error = res.responseJSON;

                    $.each(error.messages, function(name, msg) {
                        $(`#${name}`).addClass('is-invalid');
                        $(`.invalid-feedback.invalid-${name}`).html(msg);
                    });
                    
                }
            }
        });
    });
</script>

@if(Session::has('create-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'GContact integration saved successfully.',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif

@endsection
