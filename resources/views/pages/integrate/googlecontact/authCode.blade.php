<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Auth Code</title>
</head>
<body>
    <div id="access-code" data-code="{{ request()->code }}"></div>
    <script>
        const authCode = window.opener.document.getElementById('auth-code');
        const accessCode = document.getElementById('access-code');

        authCode.setAttribute('data-code', accessCode.dataset.code);
        window.close();
    </script>
</body>
</html>
