<input type="hidden" name="redirect_uri" value="{{ rtrim(config('app.url'), '/') }}/integrate/google-contact/auth">
<div class="form-group">
    <label for="application_name"><span class="text-danger">*</span>Application Name</label>
    <input type="text" class="form-control @error('application_name') is-invalid @enderror" id="application_name" name="application_name" value="{{ old('application_name') ?? $contact->application_name ?? '' }}">
    <div class="invalid-feedback invalid-application_name"></div>
</div>
<div class="form-group">
    <label for="client_id"><span class="text-danger">*</span>Client ID</label>
    <input type="text" class="form-control @error('client_id') is-invalid @enderror" id="client_id" name="client_id" value="{{ old('client_id') ?? $contact->client_id ?? '' }}">
    <div class="invalid-feedback invalid-client_id"></div>
</div>
<div class="form-group">
    <label for="client_secret"><span class="text-danger">*</span>Client Secret</label>
    <input type="text" class="form-control @error('client_secret') is-invalid @enderror" id="client_secret" name="client_secret" value="{{ old('client_secret') ?? $contact->client_secret ?? '' }}">
    <div class="invalid-feedback invalid-client_secret"></div>
</div>
<div class="form-group">
    <label for="redirect_uri"><span class="text-danger">*</span>Redirect URI</label>
    <input type="text" class="form-control @error('redirect_uri') is-invalid @enderror" id="redirect_uri" name="redirect_uri" value="{{ rtrim(config('app.url'), '/') }}/integrate/google-contact/auth" disabled>
    <div class="invalid-feedback invalid-redirect_uri"></div>
</div>
<div class="form-group d-flex justify-content-between">
    <a href="{{ route('contacts.index') }}" class="btn btn-outline-secondary">Back</a>
    <button type="submit" class="btn btn-primary" id="btn-save-gauth">Authorize</button>
</div>
