@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-envelope"></i>
        {{ __('pages.kirimemails') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item active">{{ __('breadcrumb.kirimemails') }}</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-header-right text-right col-md-6 col-12">
    @can('kirim-email-delete')
    <button type="button" class="btn btn-danger text-white btn-bulk-destroy" disabled>
        <i class="la la-trash"></i> {{ __('button.bulk_destroy') }}
    </button>
    @endcan

    @can('kirim-email-create')
    <a href="{{ route('kirim-email.create') }}" class="btn btn-primary text-white">
        <i class="la la-plus-circle"></i> {{ __('button.create') }}
    </a>
    @endcan
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-body">
            <table class="table datatable" width="100%"></table>
        </div>
    </div>
</div>

<form id="bulk-destroy" action="{{ route('kirim-email.bulk_destroy') }}" method="post" class="d-none">
    @csrf
    @method('delete')
    <input type="text" name="kirimemails">
</form>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/tables/datatable/select.dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

<script type="text/javascript">
    var selected = [];

    $(document).ready(function() {
        $('.datatable').DataTable({
            width: '100%',
            processing: true,
            select: {
                style: 'os',
                selector: 'td:first-child .select-checkbox'
            },
            ajax: {
                url: '/integrate/kirim-email?type=json',
                dataSrc: (data) => {
                    return data;
                }
            },
            order: [
                [1, 'asc']
            ],
            columns: [{
                defaultContent: '',
                title: '',
                orderable: false,
                className: 'select-checkbox pr-1 pl-2',
                width: '10px'
            }, {
                data: 'username',
                title: 'Username',
                orderable: true,
                width: "20%"
            }, {
                data: 'token',
                title: 'Token',
                orderable: false,
                className: 'text-muted',
                render: (data, type, rows) => {
                    if (data.length > 50)
                        return data.substr(0, 49) + "...";

                    return data;
                },
            }],
            rowCallback: (row, data, index) => {
                <?php if (Auth::user()->can('kirim-email-update')) { ?>

                    $('td:first-child', row).on('click', function() {
                        if (!$(row).hasClass('selected')) {
                            $(row).addClass('selected');
                            selected.push(data);
                        } else {
                            $(row).removeClass('selected');
                            selected.splice(selected.indexOf(data.id), 1);
                        }

                        if (selected.length > 0) {
                            $('.btn-bulk-destroy').removeAttr('disabled');
                        } else {
                            $('.btn-bulk-destroy').attr('disabled', 'disabled');
                        }
                    });

                    $('td', row).on('dblclick', () => {
                        window.location.href = "/integrate/kirim-email/" + data.encryptid + "/edit";
                    });
                <?php } ?>
            }
        });
    });

    $('.btn-bulk-destroy').on('click', () => {
        Swal.fire({
            title: 'Are you sure?',
            text: "Are you going to bulk destroy selected data?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-secondary',
            confirmButtonText: 'Yes, destroy it!',
        }).then((result) => {
            $('#bulk-destroy input[name="kirimemails"]').val(JSON.stringify(selected));
            $('#bulk-destroy').submit();
        });
    });
</script>

@if(Session::has('destroy-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Action destroy integrate kirim.email is successfully.',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif

@if(Session::has('bulk-destroy-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Action bulk destroy integare kirim.email is successfully.',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif
@endsection