@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-whatsapp"></i>
        {{ __('pages.whatsapps') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('whatsapp.index') }}">{{ __('breadcrumb.whatsapps') }}</a></li>
                <li class="breadcrumb-item active">Create</li>
            </ol>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <form action="{{ route('whatsapp.store') }}" method="post">
                @csrf

                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('whatsapp.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        <label for="phone_number" class="col-md-2 text-md-right" style="padding: .75rem 0;">Phone Number :</label>
                        <div class="col-md-5">
                            <input type="text" name="phone_number" id="phone_number" value="{{ old('phone_number') }}" class="form-control @error('phone_number') is-invalid @enderror" />

                            @error('phone_number')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="api_url" class="col-md-2 text-md-right" style="padding: .75rem 0;">Api URL :</label>
                        <div class="col-md-5">
                            <input type="text" name="api_url" id="api_url" value="{{ old('api_url') }}" class="form-control @error('api_url') is-invalid @enderror" />

                            @error('api_url')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="api_key" class="col-md-2 text-md-right" style="padding: .75rem 0;">Api Key :</label>
                        <div class="col-md-5">
                            <input type="text" name="api_key" id="api_key" value="{{ old('api_key') }}" class="form-control @error('api_key') is-invalid @enderror" />

                            @error('api_key')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('whatsapp.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection