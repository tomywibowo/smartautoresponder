@extends('layouts.main')

@section('header')
<div class="content-header-left col-md-6 col-12 mb-2 breadcrumb-new">
    <h3 class="content-header-title mb-0 d-inline-block">
        <i class="la la-whatsapp"></i>
        {{ __('pages.whatsapps') }}
    </h3>
    <div class="row breadcrumbs-top d-inline-block">
        <div class="breadcrumb-wrapper col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('breadcrumb.home') }}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('whatsapp.index') }}">{{ __('breadcrumb.whatsapps') }}</a></li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
        </div>
    </div>
</div>
<div class="content-header-right text-right col-md-6 col-12">
    @can('whatsapp-update')
    <button type="button" class="btn btn-cancel btn-secondary text-white d-none">
        <i class="la la-refresh"></i> {{ __('button.cancel') }}
    </button>
    <button type="button" class="btn btn-edit btn-secondary text-white">
        <i class="la la-edit"></i> {{ __('button.edit') }}
    </button>
    @endcan

    @can('whatsapp-delete')
    <button type="button" class="btn btn-destroy btn-danger text-white">
        <i class="la la-trash"></i> {{ __('button.destroy') }}
    </button>
    @endcan

    @can('whatsapp-create')
    <a href="{{ route('whatsapp.create') }}" class="btn btn-primary text-white">
        <i class="la la-plus-circle"></i> {{ __('button.create') }}
    </a>
    @endcan
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <form action="{{ route('whatsapp.update', encrypt($whatsapp->id)) }}" method="post">
                @csrf
                @method('put')

                <div class="card-header">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('whatsapp.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" class="btn btn-primary btn-save" disabled>
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group row">
                        <label for="phone_number" class="col-md-2 text-md-right" style="padding: .75rem 0;">Phone Number :</label>
                        <div class="col-md-5">
                            <input type="text" name="phone_number" id="phone_number" value="{{ old('phone_number') ? old('phone_number') : $whatsapp->phone_number }}" class="form-control @error('phone_number') is-invalid @enderror" readonly />

                            @error('phone_number')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="api_url" class="col-md-2 text-md-right" style="padding: .75rem 0;">Api URL :</label>
                        <div class="col-md-5">
                            <input type="text" name="api_url" id="api_url" value="{{ old('api_url') ? old('api_url') : $whatsapp->api_url }}" class="form-control @error('api_url') is-invalid @enderror" readonly />

                            @error('api_url')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="api_key" class="col-md-2 text-md-right" style="padding: .75rem 0;">Api Key :</label>
                        <div class="col-md-5">
                            <input type="text" name="api_key" id="api_key" value="{{ old('api_key') ? old('api_key') : $whatsapp->api_key }}" class="form-control @error('api_key') is-invalid @enderror" readonly />

                            @error('api_key')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('whatsapp.index') }}" class="btn btn-secondary">
                                <i class="la la-arrow-left"></i>
                            </a>
                            <button type="submit" class="btn btn-primary btn-save" disabled>
                                <i class="la la-save"></i> {{ __('button.save') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<form id="destroy-action" action="{{ route('whatsapp.destroy', encrypt($whatsapp->id)) }}" method="post" class="d-none">
    @csrf
    @method('delete')
</form>
@endsection

@section('stylesheet')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/css/extensions/sweetalert2.min.css') }}">
@endsection

@section('javascript')
<script src="{{ asset('assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        'use strict';

        $(".select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });
    });

    $('.btn-edit').on('click', () => {
        $('.btn-edit').addClass('d-none');
        $('.btn-cancel').removeClass('d-none');
        $('.btn-save').removeAttr('disabled');

        $('input').removeAttr('readonly');

        $('.roles-values').addClass('d-none');
        $('.roles-selection').removeClass('d-none');
    });

    $('.btn-cancel').on('click', () => {
        $('.btn-edit').removeClass('d-none');
        $('.btn-cancel').addClass('d-none');
        $('.btn-save').attr('disabled', 'disabled');

        $('input').attr('readonly', 'readonly');

        $('.roles-values').removeClass('d-none');
        $('.roles-selection').addClass('d-none');
    });

    $('.btn-destroy').on('click', () => {
        Swal.fire({
            title: 'Are you sure?',
            text: "Are you going to destroy this data?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-secondary',
            confirmButtonText: 'Yes, destroy it!',

        }).then((result) => {
            if (result.isConfirmed) {
                $('#destroy-action').submit();
            }
        })
    });
</script>

@if(Session::has('store-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Action create new integrate whatsapp is successfully.',
            confirmButtonClass: 'btn btn-primary',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif

@if(Session::has('update-success'))
<script type="text/javascript">
    $(document).ready(function() {
        Swal.fire({
            icon: 'success',
            title: 'Success',
            text: 'Action update integrate whatsapp is successfully.',
            confirmButtonClass: 'btn btn-primary',
            showConfirmButton: false,
            buttonsStyling: false,
            timer: 2000,
            timerProgressBar: true,
        });
    });
</script>
@endif
@endsection