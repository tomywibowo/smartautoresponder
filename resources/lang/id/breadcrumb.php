<?php

return [
    "home" => "Home",
    "products" => "Products",
    "contacts" => "Contacts",
    "whatsapps" => "Whatsapps",
    "kirimemails" => "Kirim.Emails",
    "googlecontacts" => "Google Contacts",
    "users" => "Users",
    "roles" => "Roles",
    "permissions" => "Permissions",
    "settings" => "Settings",
    "helps" => "Helps",
];
