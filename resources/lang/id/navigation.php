<?php

return [
    'dashboard' => 'Dashboard',
    'product-form' => 'Product Form',
    'whatsapp' => 'Wahstapp',
    'send-message' => 'Send Message',
    'broadcast' => [
        "parent" => "Broadcast",
        "create" => "Create Broadcast",
        "history" => "History Broadcast",
        "schedule" => "Schedule Broadcast"
    ],
    'integrate' => 'Integrate',
    'kirim.email' => 'Kirim.Email',
    'google-contact' => 'Google Contact',
    'contact' => 'Contact Items',
    'other' => 'Others',
    'user' => 'Users',
    'user.list' => 'List',
    'role' => 'Roles',
    'permission' => 'Permissions',
    'help' => 'Help',
    'setting' => 'Setting'
];
