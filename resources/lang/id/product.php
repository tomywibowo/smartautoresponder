<?php

return [
    'field_product_name' => 'Product Name',
    'field_product_name_note' => 'will appear on the cart & invoice',
    'field_checkout_page_url' => 'Checkout Page URL',
    'field_product_code' => 'Product Code',
    'field_product_status' => 'Product Status',
    'field_product_status_opt1' => 'Disabled',
    'field_product_status_opt2' => 'Test Mode',
    'field_product_status_opt3' => 'Live',
    'field_product_description' => 'Description',

    'fields' => [
        'images' => [
            "max_length" => "Maximum image product 5 file",
            "drop_file" => "Select Product Images To Upload"
        ]
    ]
];
