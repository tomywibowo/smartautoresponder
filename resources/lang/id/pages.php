<?php

return [
    "products" => "Products",
    "contacts" => "Contacts",
    "whatsapps" => "Whatsapps",
    "kirimemails" => "Kirim.Emails",
    "googlecontacts" => "Google Contacts",
    "users" => "Users",
    "permissions" => "Permissions",
    "settings" => "Settings",
    "help" => "Helps"
];
