<?php

return [
    "create" => "Create New",
    "save" => "Save",
    "edit" => "Edit",
    "destroy" => "Destroy",
    "bulk_destroy" => "Bulk Destroy",
    "cancel" => "Cancel",
    "back" => "Back",
    "next" => "Next",
    "previous" => "Previous"
];
