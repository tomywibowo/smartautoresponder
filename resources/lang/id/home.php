<?php

return [
    'today' => 'Today',
    'yesterday' => 'Yesterday',
    'this.week' => 'This Week',
    'this.month' => 'This Month',
    'total' => 'Total'
];
