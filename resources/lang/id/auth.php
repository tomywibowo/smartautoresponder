<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'logout' => 'Logout',

    'login-heading' => 'Please insert your credentials',
    
    'username-label' => "Your E-Mail or Username",
    'password-label' => "Enter Password",
    'remember-label' => "Remember Me",

    'forgot-password-link' => 'Forgot Password?',
    
    'btn-login-text' => 'Login'
];
