<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{ContactController, HelpController, ProductController, HomeController, PermissionController, ProfileController, RoleController, SettingController, UserController, PreviewThemeProductController};
use App\Http\Controllers\Integrate\GoogleContactController;
use App\Http\Controllers\Integrate\KirimEmailController;
use App\Http\Controllers\Integrate\WhatsappController;
use App\Http\Controllers\Whatsapp\BroadcastController;

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');

    Route::resource('/products', ProductController::class)->except('show');
    Route::prefix('products')->name('products.')->group(function () {
        Route::delete('/', [ProductController::class, 'bulk_destroy'])->name('bulk_destroy');

        // Product's images routes
        Route::prefix('/{id}/images')->group(function () {
            Route::get('/create', [ProductController::class, 'create_images'])->name('create_images');
            Route::post('/create', [ProductController::class, 'store_images']);
            Route::get('/edit', [ProductController::class, 'edit_images'])->name('edit_images');
            Route::put('/edit', [ProductController::class, 'update_images']);
        });

        // Product's pricing routes
        Route::prefix('/{id}/pricings')->group(function () {
            Route::get('/create', [ProductController::class, 'create_pricings'])->name('create_pricings');
            Route::post('/create', [ProductController::class, 'store_pricings']);
            Route::get('/edit', [ProductController::class, 'edit_pricings'])->name('edit_pricings');
            Route::put('/edit', [ProductController::class, 'update_pricings']);
        });

        // Product's bump routes
        Route::prefix('/{id}/bumps')->group(function () {
            Route::get('/create', [ProductController::class, 'create_bumps'])->name('create_bumps');
            Route::post('/create', [ProductController::class, 'store_bumps']);
            Route::get('/edit', [ProductController::class, 'edit_bumps'])->name('edit_bumps');
            Route::put('/edit', [ProductController::class, 'update_bumps']);
        });

        // Product's shipping routes
        Route::prefix('/{id}/shippings')->group(function () {
            Route::get('/create', [ProductController::class, 'create_shippings'])->name('create_shippings');
            Route::post('/create', [ProductController::class, 'store_shippings']);
            Route::get('/edit', [ProductController::class, 'edit_shippings'])->name('edit_shippings');
            Route::put('/edit', [ProductController::class, 'update_shippings']);
        });

        // Product's payment routes
        Route::prefix('/{id}/payments')->group(function () {
            Route::get('/create', [ProductController::class, 'create_payments'])->name('create_payments');
            Route::post('/create', [ProductController::class, 'store_payments']);
            Route::get('/edit', [ProductController::class, 'edit_payments'])->name('edit_payments');
            Route::put('/edit', [ProductController::class, 'update_payments']);
        });

        // Product's unique code routes
        Route::prefix('/{id}/unique_codes')->group(function () {
            Route::get('/create', [ProductController::class, 'create_unique_code'])->name('create_unique_codes');
            Route::post('/create', [ProductController::class, 'store_unique_code']);
            Route::get('/edit', [ProductController::class, 'edit_unique_code'])->name('edit_unique_codes');
            Route::put('/edit', [ProductController::class, 'update_unique_code']);
        });

        // Product's checkout page routes
        Route::prefix('/{id}/checkout-page')->group(function () {
            Route::get('create', [ProductController::class, 'create_checkout_page'])->name('create_checkout_page');
        });
    });

    Route::prefix('whatsapp')->group(function () {
        Route::delete('/broadcasts', [BroadcastController::class, 'bulk_destroy'])->name('broadcasts.bulk_destroy');
        Route::get('/broadcasts/history', [BroadcastController::class, 'list'])->name('broadcasts.history');
        Route::get('/broadcast/schedule', [BroadcastController::class, 'schedule'])->name('broadcasts.schedule');
        Route::delete('/broadcast/schedule', [BroadcastController::class, 'bulk_destroy_schedule']);
        Route::resource('/broadcasts', BroadcastController::class)->except('edit', 'update');
    });

    Route::prefix('integrate')->group(function () {
        Route::delete('/whatsapp', [WhatsappController::class, 'bulk_destroy'])->name('whatsapp.bulk_destroy');
        Route::resource('/whatsapp', WhatsappController::class)->except('show');

        Route::delete('/kirim-email', [KirimEmailController::class, 'bulk_destroy'])->name('kirim-email.bulk_destroy');
        Route::resource('/kirim-email', KirimEmailController::class)->except('show');

        Route::get('/google-contact/auth', function () {
            return view('pages.integrate.googlecontact.authCode');
        });
        Route::post('/google-contact/auth', [GoogleContactController::class, 'auth'])->name('google-contact.auth');
        Route::post('/google-contact/fetch-code', [GoogleContactController::class, 'fetchCode'])->name('google-contact.fetchCode');
        Route::delete('/google-contact', [GoogleContactController::class, 'bulk_destroy'])->name('google-contact.bulk_destroy');
        Route::resource('/google-contact', GoogleContactController::class)->except('show');
    });

    Route::delete('/contacts', [ContactController::class, 'bulk_destroy'])->name('contacts.bulk_destroy');
    Route::resource('/contacts', ContactController::class)->except('show');

    Route::delete('/users', [UserController::class, 'bulk_destroy'])->name('users.bulk_destroy');
    Route::resource('/users', UserController::class)->except('show');

    Route::delete('/roles', [RoleController::class, 'bulk_destroy'])->name('roles.bulk_destroy');
    Route::resource('/roles', RoleController::class)->except('show');

    Route::resource('/permissions', PermissionController::class)->only('index');

    Route::resource('/settings', SettingController::class)->only('index', 'udpate');

    Route::get('help', [HelpController::class, 'index'])->name('help.index');

    Route::get('preview-product', [PreviewThemeProductController::class, 'index']);
});
