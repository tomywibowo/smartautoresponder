<?php

namespace App\Http\Controllers\Integrate;

use App\Helpers\GContact;
use App\Http\Controllers\Controller;
use App\Http\Requests\GoogleContactRequest;
use App\Models\GoogleContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class GoogleContactController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:google-contact-read|google-contact-create|google-contact-update|google-contact-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:google-contact-create', ['only' => ['create', 'store', 'auth']]);
        $this->middleware('permission:google-contact-update', ['only' => ['edit', 'update']]);
        $this->middleware('permission:google-contact-delete', ['only' => ['destroy', 'bulk_destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $index = 0;
        $gContacts = [];
        $all = GoogleContact::all();
        foreach ($all as $item) {
            $gContacts[$index] = $item;
            $gContacts[$index]["encryptid"] = encrypt($item->id);
            $index++;
        }

        if (request()->get('type') == "json") {
            return response()->json($gContacts);
        }

        return view('pages.integrate.googlecontact.index', compact('gContacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.integrate.googlecontact.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GoogleContactRequest $request)
    {
        GoogleContact::create($request->all());

        return redirect()->back()->with('create-success', true);
    }

    public function auth(Request $request)
    {
        $gContact = new GContact();

        $validator = Validator::make($request->all(), [
            'application_name' => 'required|string',
            'client_id' => 'required|string',
            'client_secret' => 'required|string',
            'redirect_uri' => 'required|url'
        ]);

        if($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'type' => 'validation',
                'messages' => $validator->errors()
            ], Response::HTTP_BAD_REQUEST);
        }

        $gContact = new GContact;
        return $gContact->generateAuthUri($request->all());
    }

    public function fetchCode(Request $request)
    {
        $gContact = new GContact;
        $fetch = $gContact->fetchAccToken($request->client, $request->accessCode);

        if($fetch['status'] == 'error') {
            return response()->json($fetch, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response()->json($fetch, Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function bulk_destroy(Request $request)
    {
        $gContacts = json_decode($request->gcontacts);
        
        foreach($gContacts as $gContact) {
            GoogleContact::destroy($gContact->id);
        }

        return redirect()->back()->with('bulk-destroy-success', 1);
    }
}
