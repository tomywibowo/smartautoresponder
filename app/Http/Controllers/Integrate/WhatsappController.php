<?php

namespace App\Http\Controllers\Integrate;

use App\Http\Controllers\Controller;
use App\Models\Whatsapp;
use Illuminate\Http\Request;

class WhatsappController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $whatsapp = [];

        $index = 0;
        $data = Whatsapp::all();
        foreach ($data as $item) {
            $whatsapp[$index] = $item;
            $whatsapp[$index]["encryptid"] = encrypt($item->id);
        }

        if (request()->get('type') == "json") {
            return response()->json($whatsapp);
        }

        return view('pages.integrate.whatsapp.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.integrate.whatsapp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "phone_number" => "required|unique:integrate_whatsapps",
            "api_url" => "required|url",
            "api_key" => "required"
        ]);

        $whatsapp = Whatsapp::create($request->all());
        return redirect()->route("whatsapp.edit", encrypt($whatsapp->id))
            ->with('store-success', 'Action create integrate whatsapp successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $whatsapp = Whatsapp::find(decrypt($id));
        return view('pages.integrate.whatsapp.edit', compact('whatsapp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "phone_number" => "required|unique:integrate_whatsapps,phone_number," . decrypt($id),
            "api_url" => "required|url",
            "api_key" => "required"
        ]);

        $whatsapp = Whatsapp::find(decrypt($id));
        $whatsapp->fill($request->all());
        $whatsapp->save();

        return redirect()->back()
            ->with('store-success', 'Action create integrate whatsapp successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $whatsapp = Whatsapp::find(decrypt($id));
        $whatsapp->delete();

        return redirect()->route('whatsapp.index')
            ->with('destroy-success', 'Action destroy integrate whatsapp is successfully');
    }

    /**
     * Remove the resource selected from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function bulk_destroy()
    {
        $whatsapps = json_decode(request()->whatsapps);
        foreach ($whatsapps as $whatsapp) {
            $data = Whatsapp::find($whatsapp->id);
            $data->delete();
        }
        return redirect()->back()
            ->with('bulk-destroy-success', 'Action bulk destroy integrate whatsapp is successfully');
    }
}
