<?php

namespace App\Http\Controllers\Integrate;

use App\Http\Controllers\Controller;
use App\Models\KirimEmail;
use Illuminate\Http\Request;

class KirimEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kirimemail = [];

        $index = 0;
        $data = KirimEmail::all();
        foreach ($data as $item) {
            $kirimemail[$index] = $item;
            $kirimemail[$index]["encryptid"] = encrypt($item->id);
        }

        if (request()->get('type') == "json") {
            return response()->json($kirimemail);
        }

        return view('pages.integrate.kirimemail.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("pages.integrate.kirimemail.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|unique:integrate_kirim_email',
            'token' => 'required'
        ]);

        $kirimemail = KirimEmail::create($request->all());
        return redirect()->route('kirim-email.edit', encrypt($kirimemail->id))
            ->with('store-success', 'Action create integrate kirim.email successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kirimemail = KirimEmail::find(decrypt($id));
        return view("pages.integrate.kirimemail.edit", compact('kirimemail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'username' => 'required|unique:integrate_kirim_email,username,' . decrypt($id),
            'token' => 'required'
        ]);

        $kirimemail = KirimEmail::find(decrypt($id));
        $kirimemail->fill($request->all());
        $kirimemail->save();

        return redirect()->back()
            ->with('store-success', 'Action create integrate kirim.email successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kirimemail = KirimEmail::find(decrypt($id));
        $kirimemail->delete();

        return redirect()->route('kirim-email.index')
            ->with('destroy-success', 'Action destroy integrate kirim.email is successfully');
    }

    /**
     * Remove the resource selected from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function bulk_destroy()
    {
        $kirimemails = json_decode(request()->kirimemails);
        foreach ($kirimemails as $kirimemail) {
            $data = KirimEmail::find($kirimemail->id);
            $data->delete();
        }
        return redirect()->back()
            ->with('bulk-destroy-success', 'Action bulk destroy integrate kirim.email is successfully');
    }
}
