<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:role-read|role-create|role-update|role-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:role-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role-update', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = [];

        $index = 0;
        $data = Role::all();
        foreach ($data as $role) {
            $roles[$index] = $role;
            $roles[$index]["encryptid"] = encrypt($role->id);
            $index++;
        }

        if (request()->get('type') == 'json') {
            return response()->json($roles);
        }

        return view('pages.users.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = [];

        $index = 0;
        $data = Permission::where('parent_id', '=', null)->orderBy('name', 'ASC')->get();
        foreach ($data as $item) {
            $permissions[$index] = $item;
            $permissions[$index]["create"] = Permission::where('parent_id', '=', $item->id)->where('name', 'like', '%create')->first();
            $permissions[$index]["update"] = Permission::where('parent_id', '=', $item->id)->where('name', 'like', '%update')->first();
            $permissions[$index]["delete"] = Permission::where('parent_id', '=', $item->id)->where('name', 'like', '%delete')->first();
            $index++;
        }

        return view('pages.users.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ]);

        $role = Role::create(['name' => $request->name]);
        $role->syncPermissions($request->permission);

        return redirect()->route('roles.edit', encrypt($role->id))
            ->with('store-success', 'Action create new roles is successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permissions = [];

        $index = 0;
        $data = Permission::where('parent_id', '=', null)->orderBy('name', 'ASC')->get();
        foreach ($data as $item) {
            $permissions[$index] = $item;
            $permissions[$index]["create"] = Permission::where('parent_id', '=', $item->id)->where('name', 'like', '%create')->first();
            $permissions[$index]["update"] = Permission::where('parent_id', '=', $item->id)->where('name', 'like', '%update')->first();
            $permissions[$index]["delete"] = Permission::where('parent_id', '=', $item->id)->where('name', 'like', '%delete')->first();
            $index++;
        }

        $role = Role::find(decrypt($id));
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", decrypt($id))
            ->pluck('role_has_permissions.permission_id')
            ->all();

        return view('pages.users.roles.edit', compact('permissions', 'role', 'rolePermissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name,' . decrypt($id),
            'permission' => 'required',
        ]);

        $role = Role::find(decrypt($id));
        $role->name = $request->name;
        $role->save();

        $role->syncPermissions($request->permission);

        return redirect()->back()
            ->with('update-success', 'Action create new roles is successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find(decrypt($id));
        $role->delete();

        return redirect()->route('roles.index')
            ->with('destroy-success', 'Action delete roles is successfully');
    }

    /**
     * Remove the resource selected from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function bulk_destroy()
    {
        $roles = json_decode(request()->roles);
        foreach ($roles as $role) {
            $data = Role::find($role->id);
            $data->delete();
        }
        return redirect()->back()
            ->with('bulk-destroy-success', 'Action bulk destroy role is successfully');
    }
}
