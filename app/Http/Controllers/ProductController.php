<?php

namespace App\Http\Controllers;

use App\Helpers\Product as ProductHelper;
use App\Helpers\Shipping;
use App\Models\Product;
use App\Models\Productbump;
use App\Models\Productimage;
use App\Models\Productpayment;
use App\Models\Productpricing;
use App\Models\Productshipping;
use App\Models\Productuniquecode;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:product-read|product-create|product-update|product-delete', ['only' => ['index', 'show']]);
        $this->middleware('permission:product-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:product-update', ['only' => ['edit', 'update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy', 'bulk_destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $index = 0;
        $products = [];
        $all = Product::all();
        foreach ($all as $item) {
            $products[$index] = $item;
            $products[$index]["encryptid"] = encrypt($item->id);
            $index++;
        }

        if (request()->get('type') == "json") {
            return response()->json($products);
        }

        return view('pages.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = [];
        return view('pages.products.create', compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required|unique:products',
            'code' => 'required|unique:products|max:8|min:2',
            'status' => 'required'
        ]);

        $results = Product::create($request->all());
        return redirect()->route('products.create_images', encrypt($results->id));
    }

    public function create_images(Request $request, $id)
    {
        $product = Product::find(decrypt($id));
        return view('pages.products.sections.create_image', compact('product'));
    }

    public function edit_images(Request $request, $id)
    {
        $product = Product::find(decrypt($id));
        return view('pages.products.sections.edit_image', compact('product'));
    }

    public function store_images(Request $request, $id)
    {
        $productHelper = new ProductHelper($request->dir);
        if($request->file('image')) {
            $images = $productHelper->uploadImages($request->file('image'), decrypt($id));
            Productimage::insert($images);
        }
       
        return redirect()->route('products.create_pricings', $id);
    }

    public function update_images(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));
        $productHelper = new ProductHelper($request->dir);

        if($request->image) {
            $request->validate([
                'image' => 'required|array',
                'image.*' => 'required|file|mimes:jpg,png,gif,jpeg,svg'
            ]);

            $files = $request->file('image');
            $images = $productHelper->uploadImages($files, $product->id);

            Productimage::insert($images);
        }

        if($images = $request->delete_images) {
            $request->validate([
                'delete_images' => 'required'
            ]);

            $images = json_decode($images);
            Productimage::wherein('id', $images)->delete();
        }

        return redirect()->back()->with('update-success', true);
    }

    public function create_pricings(Request $request, $id)
    {
        $product = Product::find(decrypt($id));

        if( $product->pricing ) {
            return redirect('products/' . encrypt($product->id) . '/pricings/edit');
        }

        return view('pages.products.sections.create_pricing', compact('product'));
    }

    public function edit_pricings(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));
        
        if( !$product->pricing ) {
            return redirect('products/' . encrypt($product->id) . '/pricings/create');
        }

        return view('pages.products.sections.edit_pricing', compact('product'));
    }

    public function store_pricings(Request $request, $id)
    {
        $request->validate([
            'regular_price' => 'required'
        ]);

        if ($request->is_sale_price) {
            $request->validate([
                'regular_price' => 'required',
                'sale_price' => 'required'
            ]);
        }

        if ($request->is_schedule_sale_price) {
            $request->validate([
                'regular_price' => 'required',
                'sale_price' => 'required',
                'schedule_sale_price' => 'required'
            ]);
        }
        
        Productpricing::create([
            'product_id' => $request->product_id,
            'regular_price' => $request->regular_price,
            'is_sale_price' => $request->is_sale_price,
            'sale_price' => $request->sale_price,
            'is_schedule_sale_price' => $request->is_schedule_sale_price,
            'schedule_sale_price' => json_encode($request->schedule_sale_price)
        ]);
        return redirect()
        ->route('products.create_bumps', $id);
    }
    
    public function update_pricings(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));

        $request->validate([
            'product_id' => 'required',
            'is_sale_price' => 'required|numeric|in:1,0',
            'is_schedule_sale_price' => 'required|numeric|in:1,0',
            'regular_price' => 'required|numeric',
        ]);

        if($request->is_sale_price) {
            $request->validate([
                'sale_price' => 'required|numeric'
            ]);
        }

        if($request->is_schedule_sale_price) {
            $request->validate([
                'schedule_sale_price.*' => 'required|date'
            ]);
        }

        $product->pricing->update($request->all());
        return redirect()->back()->with('update-success', true);
    }

    public function create_bumps(Request $request, $id)
    {
        $product = Product::find(decrypt($id));

        if( $product->bump ) {
            return redirect('products/' . encrypt($product->id) . '/bumps/edit');
        }

        return view('pages.products.sections.create_bumps', compact('product'));
    }

    public function edit_bumps(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));

        if( !$product->bump ) {
            return redirect('products/' . encrypt($product->id) . '/bumps/create');
        }

        return view('pages.products.sections.edit_bumps', compact('product'));
    }

    public function store_bumps(Request $request, $id)
    {        
        if($request->is_bump) {
            $request->validate([
                'product_id' => 'required',
                'name' => 'required',
                'price' => 'required|numeric',
                'title' => 'required',
                'description' => 'required'
            ]);

            Productbump::create([
                'product_id' => decrypt($id),
                'name' => $request->name,
                'price' => $request->price,
                'title' => $request->title,
                'description' => $request->description,
            ]);
        }

        return redirect()->route('products.create_shippings', $id);
    }

    public function update_bumps(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));

        if($request->is_bump) {
            $request->validate([
                'product_id' => 'required|numeric',
                'name' => 'required',
                'price' => 'required|numeric',
                'title' => 'required',
                'description' => 'required'
            ]);

            if($product->bump) {
                $product->bump->update($request->all());
            } else {
                Productbump::create($request->all());
            }

            return redirect()->back()->with('update-success', true);
        }

        if($product->bump) {
            $product->bump->delete();
        }

        return redirect()->back()->with('update-success', true);
    }


    public function create_shippings(Request $request, $id)
    {
        $shipping = new Shipping;
        $product = Product::findOrFail(decrypt($id));
        $provinces = $shipping->getProvince();

        if( $product->shipping ) {
            return redirect('products/' . encrypt($product->id) . '/shippings/edit');
        }

        return view('pages.products.sections.create_shippings', compact('product', 'provinces'));
    }

    public function edit_shippings(Request $request, $id)
    {
        $shipping = new Shipping;
        $product = Product::findOrFail(decrypt($id));
        $provinces = $shipping->getProvince();

        if( !$product->shipping ) {
            return redirect('products/' . encrypt($product->id) . '/shippings/create');
        }

        return view('pages.products.sections.edit_shippings', compact('product', 'provinces'));
    }

    public function store_shippings(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));
        
        $request->validate([
            'product_id' => 'required|numeric',
            'weight' => 'required|numeric',
            'curiers' => 'required|array',
            'curiers.*' => 'required|string',
            'ship_from' => 'required|array|min:2',
            'ship_from.*' => 'required|numeric',
        ]);

        Productshipping::create($request->all());
        return redirect()->route('products.create_payments', $id);
    }

    public function update_shippings(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));
        
        $request->validate([
            'weight' => 'required|numeric',
            'curiers' => 'required|array',
            'curiers.*' => 'required|string',
            'ship_from' => 'required|array|min:2',
            'ship_from.*' => 'required|numeric',
        ]);

        $product->shipping->update($request->all());
        return redirect()->back()->with('update-success', true);
    }

    public function create_payments(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));

        if( $product->payments ) {
            return redirect('products/' . encrypt($product->id) . '/payments/edit');
        }

        return view('pages.products.sections.create_payments', compact('product'));
    }

    public function edit_payments(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));

        if( !$product->payments ) {
            return redirect('products/' . encrypt($product->id) . '/payments/create');
        }

        return view('pages.products.sections.edit_payments', compact('product'));
    }

    public function store_payments(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));

        if($request->payment) {
            $request->validate([
                'product_id' => 'required|numeric',
                'payment' => 'required|array',
                'payment.*' => 'required|string'
            ]);

            Productpayment::create($request->all());
        }

        return redirect()->route('products.create_unique_codes', $id);
    }

    public function update_payments(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));

        if($request->payment) {
            $request->validate([
                'product_id' => 'required|numeric',
                'payment' => 'required|array',
                'payment.*' => 'required|string'
            ]);
        }

        $product->payments->update([
            'payment' => $request->payment ?? []
        ]);

        return redirect()->back()->with('update-success', true);
    }

    public function create_unique_code(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));

        if( $product->uniqueCode ) {
            return redirect('products/' . encrypt($product->id) . '/unique_codes/edit');
        }

        return view('pages.products.sections.create_unique_codes', compact('product'));
    }

    public function edit_unique_code(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));

        if( !$product->uniqueCode ) {
            return redirect('products/' . encrypt($product->id) . '/unique_codes/create');
        }

        return view('pages.products.sections.edit_unique_codes', compact('product'));
    }

    public function store_unique_code(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));
        
        if($request->is_unique_code) {
            $request->validate([
                'product_id' => 'required|numeric',
                'type' => 'required|in:decrease,increase',
                'range' => 'required|array|min:2',
                'range.*' => 'required|numeric'
            ]);

            Productuniquecode::create($request->all());
        }

        return redirect()->route('products.edit_unique_codes', $id);
    }

    public function update_unique_code(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));
        
        if($request->is_unique_code) {
            $request->validate([
                'product_id' => 'required|numeric',
                'type' => 'required|in:decrease,increase',
                'range' => 'required|array|min:2',
                'range.*' => 'required|numeric'
            ]);

            if($product->uniqueCode) {
                $product->uniqueCode->update($request->all());
            } else {
                Productuniquecode::create($request->all());
            }
        } else {
            $product->uniqueCode->delete();
        }
        
        return redirect()->back()->with('update-success', true);
    }

    public function create_checkout_page($id)
    {
        $product = Product::findOrFail(decrypt($id));

        return view('pages.products.create_checkout', compact('product'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find(decrypt($id));

        return view('pages.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail(decrypt($id));

        $this->validate($request, [
            'name' => 'required',
            'status' => 'required'
        ]);
        
        if($request->url != $product->url) {
            $this->validate($request, [
                'url' => 'required|unique:products'
            ]);
        }
        
        if($request->code != $product->code) {
            $this->validate($request, [
                'code' => 'required|unique:products|max:8|min:2',
            ]);
        }

        $product->update($request->all());
        
        return redirect()->back()->with('update-success', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove products
     */
    public function bulk_destroy(Request $request)
    {
        $products = json_decode($request->products);
        
        foreach($products as $product) {
            Product::destroy($product->id);
        }

        return redirect()->back()->with('bulk-destroy-success', 1);
    }
}
