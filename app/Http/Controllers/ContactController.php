<?php

namespace App\Http\Controllers;

use App\Helpers\GContact;
use App\Models\Contact;
use Illuminate\Http\Request;
use League\CommonMark\Context;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $index = 0;
        $products = [];
        $all = Contact::all();
        foreach ($all as $item) {
            $products[$index] = $item;
            $products[$index]["encryptid"] = encrypt($item->id);
            $index++;
        }

        if (request()->get('type') == "json") {
            return response()->json($products);
        }

        return view('pages.contacts.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required|min:11|numeric|unique:contacts,phone',
            'email' => 'required|email|unique:contacts,email'
        ]);

        $contact = Contact::create($request->all());
        $gContact = new GContact;

        $gContact->saveContact($contact);
        return redirect()->back()->with('create-success', true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::findOrFail(decrypt($id));

        return view('pages.contacts.edit', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::findOrFail(decrypt($id));

        $request->validate([
            'name' => 'required',
            'phone' => 'required|min:11|numeric',
            'email' => 'required|email'
        ]);

        if($request->phone != $contact->phone) {
            $request->validate([
                'phone' => 'unique:contacts,phone'
            ]);
        }

        if($request->email != $contact->email) {
            $request->validate([
                'email' => 'unique:contacts,email'
            ]);
        }

        $contact->update($request->all());
        return redirect()->back()->with('update-success', true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function bulk_destroy(Request $request)
    {
        $contacts = json_decode($request->contacts);
        
        foreach($contacts as $contact) {
            Contact::destroy($contact->id);
        }

        return redirect()->back()->with('bulk-destroy-success', 1);
    }
}
