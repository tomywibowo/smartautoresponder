<?php

namespace App\Http\Controllers\Whatsapp;

use App\Helpers\WhatsApp as WhatsAppHelper;
use App\Http\Controllers\Controller;
use App\Models\BroadcastSchedule;
use App\Models\Contact;
use App\Models\Whatsapp;
use App\Models\WhatsAppBroadcast;
use Illuminate\Http\Request;

class BroadcastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $whatsapp = new WhatsAppHelper;
        $senders = Whatsapp::get();
        $dests = Contact::get();

        return view('pages.whatsapps.broadcast', compact('senders', 'dests'));
    }

    public function list()
    {
        $index = 0;
        $products = [];
        $all = WhatsAppBroadcast::all();
        foreach ($all as $item) {
            $products[$index] = $item;
            $products[$index]["encryptid"] = encrypt($item->id);
            $index++;
        }

        if (request()->get('type') == "json") {
            return response()->json($products);
        }

        return view('pages.whatsapps.broadcast-history');
    }

    public function schedule()
    {
        $index = 0;
        $products = [];
        $all = BroadcastSchedule::all();
        foreach ($all as $item) {
            $products[$index] = $item;
            $products[$index]["encryptid"] = encrypt($item->id);
            $products[$index]['due'] = date('Y-m-d H:i:s', $item->due);
            $index++;
        }

        if (request()->get('type') == "json") {
            return response()->json($products);
        }

        return view('pages.whatsapps.broadcast-schedule');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dests = '*';

        $request->validate([
            'sender' => 'required|numeric|exists:integrate_whatsapps,phone_number',
            'message' => 'required|string',
            'date' => 'required|date_format:Y-m-d',
            'time' => 'required|date_format:H:i',
        ]);

        if($request->period) {
            $request->validate([
                'period' => 'required|in:hourly,daily,weekly,monthly,annual'
            ]);
        }

        if (!$request->dests_all) {
            $request->validate([
                'destinations' => 'required|array',
                'destinations.*' => 'required|numeric|exists:contacts,phone'
            ]);

            $dests = $request->destinations;
        }

        $whatsapp = new WhatsAppHelper;
        $sender = Whatsapp::where('phone_number', $request->sender)->first();
        $send = $whatsapp->addBroadcast($sender, $dests, $request);

        if ($send['status'] == true) {
            return redirect()->back()
                ->with('has-sent', true)
                ->with('message', $send['message']);
        }

        return redirect()->back()
            ->with('has-sent', false)
            ->with('message', $send['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function bulk_destroy(Request $request)
    {
        $broadcasts = json_decode($request->broadcasts);

        foreach ($broadcasts as $product) {
            WhatsAppBroadcast::destroy($product->id);
        }

        return redirect()->back()->with('bulk-destroy-success', 1);
    }

    public function bulk_destroy_schedule(Request $request)
    {
        $schedules = json_decode($request->schedules);

        foreach ($schedules as $schedule) {
            BroadcastSchedule::destroy($schedule->id);
        }

        return redirect()->back()->with('bulk-destroy-success', 1);
    }
}
