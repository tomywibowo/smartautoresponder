<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Shipping;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    public function getCity($pid)
    {
        $shipping = new Shipping;
        $cities = $shipping->getCity($pid);

        return response()->json([
            'status' => 'ok',
            'cities' => $cities
        ], Response::HTTP_OK);
    }
}
