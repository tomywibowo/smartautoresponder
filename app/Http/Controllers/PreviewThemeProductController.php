<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;

class PreviewThemeProductController extends Controller
{
    public function index(Request $request)
    {
        $data = [
            'product'   => $request->product_id ? Product::find($request->product_id) : null
        ];

        if( $request->get('product_template') ) {
            return view('product-templates.' . $request->get('product_template'), $data);
        }

        return view('product-templates.right-sidebar', $data);
    }
}
