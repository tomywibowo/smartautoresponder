<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:permission-read', ['only' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $index = 0;
        $permissions = [];
        $data = Permission::where('parent_id', '=', null)->orderBy('name', 'ASC')->get();
        foreach ($data as $item) {
            $permissions[$index] = $item;
            $permissions[$index]["create"] = Permission::where('parent_id', '=', $item->id)->where('name', 'like', '%create')->first();
            $permissions[$index]["update"] = Permission::where('parent_id', '=', $item->id)->where('name', 'like', '%update')->first();
            $permissions[$index]["delete"] = Permission::where('parent_id', '=', $item->id)->where('name', 'like', '%delete')->first();
            $index++;
        }

        if (request()->get('type') == "json") {
            return response()->json($permissions);
        }

        return view('pages.users.permissions.index', compact('permissions'));
    }
}
