<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GoogleContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'application_name' => 'required|string',
            'client_id' => 'required|string',
            'client_secret' => 'required|string',
            'redirect_uri' => 'required',
            'access_token' => 'required|string',
            'created' => 'required|numeric',
            'expires_in' => 'required|numeric',
            'refresh_token' => 'required|string',
            'scope' => 'required|string',
            'token_type' => 'required|string'
        ];
    }
}
