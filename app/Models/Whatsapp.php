<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Whatsapp extends Model
{
    use HasFactory;

    /**
     * The table choose.
     *
     * @var String
     */
    protected $table = "integrate_whatsapps";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "phone_number", "api_url", "api_key"
    ];
}
