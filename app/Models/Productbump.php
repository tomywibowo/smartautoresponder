<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productbump extends Model
{
    use HasFactory;

    protected $table = "product_bumps";

    protected $fillable = [
        "product_id",
        "name",
        "price",
        "title",
        "subtitle",
        "description",
        "html"
    ];
}
