<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KirimEmail extends Model
{
    use HasFactory;

    /**
     * The table choose.
     *
     * @var String
     */
    protected $table = "integrate_kirim_email";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "username", "token"
    ];
}
