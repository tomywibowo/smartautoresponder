<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productheader extends Model
{
    use HasFactory;

    protected $table = "product_headers";

    protected $fillable = [
        "product_id",
        "image",
        "tagline"
    ];
}
