<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productpayment extends Model
{
    use HasFactory;

    protected $table = "product_payments";

    protected $fillable = [
        "product_id",
        "payment"
    ];

    protected $casts = [
        'payment' => 'object'
    ];
}
