<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producttemplate extends Model
{
    use HasFactory;

    protected $table = "product_templates";

    protected $fillable = [
        "product_id",
        "layout",
        "background_color"
    ];
}
