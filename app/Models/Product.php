<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = "products";

    protected $fillable = [
        "name",
        "url",
        "code",
        "status",
        "description"
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function($product) {
            $product->images->each->delete();
            $product->pricing->delete();
            $product->bump->delete();
        });
    }

    public function images()
    {
        return $this->hasMany(Productimage::class);
    }

    public function pricing()
    {
        return $this->hasOne(Productpricing::class);
    }

    public function bump()
    {
        return $this->hasOne(Productbump::class);
    }

    public function shipping()
    {
        return $this->hasOne(Productshipping::class);
    }

    public function uniqueCode()
    {
        return $this->hasOne(Productuniquecode::class);
    }

    public function payments()
    {
        return $this->hasOne(Productpayment::class);
    }
}
