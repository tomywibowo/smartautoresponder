<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoogleContact extends Model
{
    use HasFactory;
    
    public $timestamps = false;
    
    protected $table = 'integrate_google_contact';

    protected $fillable = [
        'application_name' , 'client_id', 'client_secret', 'redirect_uri', 'access_token', 'expires_in', 'refresh_token', 'scope', 'token_type', 'created'
    ];

}
