<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paymentmethod extends Model
{
    use HasFactory;

    protected $table = "payment_methods";

    protected $fillable = [
        "product_id",
        "code",
        "name"
    ];
}
