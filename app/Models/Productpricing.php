<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productpricing extends Model
{
    use HasFactory;

    protected $table = "product_pricings";

    protected $fillable = [
        "product_id",
        "regular_price",
        "is_sale_price",
        "sale_price",
        "is_schedule_sale_price",
        "schedule_sale_price"
    ];

    protected $casts = [
        "schedule_sale_price" => 'object'
    ];
}
