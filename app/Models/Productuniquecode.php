<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productuniquecode extends Model
{
    use HasFactory;

    protected $table = "product_unique_codes";

    protected $fillable = [
        "product_id",
        "type",
        "range"
    ];

    protected $casts = [
        'range' => 'object'
    ];
}
