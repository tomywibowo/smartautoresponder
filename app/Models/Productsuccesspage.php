<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productsuccesspage extends Model
{
    use HasFactory;

    protected $table = "product_success_pages";

    protected $fillable = [
        "product_id",
        "success_type",
        "content"
    ];
}
