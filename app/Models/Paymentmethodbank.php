<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Paymentmethodbank extends Model
{
    use HasFactory;

    protected $table = "payment_method_banks";

    protected $fillable = [
        "payment_method_id",
        "bank_name",
        "bank_account",
        "bank_number"
    ];
}
