<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BroadcastSchedule extends Model
{
    use HasFactory;

    protected $table = 'whatsapp_broadcast_schedules';

    protected $fillable = [
        'sender', 'destinations', 'message', 'period', 'due'
    ];

    protected $casts = [
        'destinations' => 'array'
    ];
}
