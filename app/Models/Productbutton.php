<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productbutton extends Model
{
    use HasFactory;

    protected $table = "product_buttons";

    protected $fillable = [
        "product_id",
        "text_value",
        "button_color"
    ];
}
