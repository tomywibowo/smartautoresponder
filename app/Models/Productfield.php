<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productfield extends Model
{
    use HasFactory;

    protected $table = "product_fields";

    protected $fillable = [
        "product_id",
        "required",
        "placeholder"
    ];
}
