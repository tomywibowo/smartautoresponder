<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productvideo extends Model
{
    use HasFactory;

    protected $table = "product_videos";

    protected $fillable = [
        "product_id",
        "url"
    ];
}
