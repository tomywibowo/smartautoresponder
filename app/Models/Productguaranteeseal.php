<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productguaranteeseal extends Model
{
    use HasFactory;

    protected $table = "product_guarantee_seals";

    protected $fillable = [
        "product_id",
        "image"
    ];
}
