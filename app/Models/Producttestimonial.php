<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producttestimonial extends Model
{
    use HasFactory;

    protected $table = "product_testimonials";

    protected $fillable = [
        "product_id",
        "image",
        "name",
        "content"
    ];
}
