<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productshipping extends Model
{
    use HasFactory;

    protected $table = "product_shippings";

    protected $fillable = [
        "product_id",
        "weight",
        "curiers",
        "ship_from",
        "is_free_shipping"
    ];

    protected $casts = [
        "curiers" => 'object',
        "ship_from" => 'object'
    ];
}
