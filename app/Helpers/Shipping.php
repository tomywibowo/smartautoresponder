<?php
namespace App\Helpers;

use ErrorException;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Storage;

class Shipping
{
    protected $baseApiUrl = "https://api.rajaongkir.com/starter/";

    protected $contentSize = 5120;

    protected $apiKey;

    public $client;

    public function __construct()
    {
        $this->apiKey = config('app.ro_apikey');

        $this->client = new GuzzleClient([
            'base_uri' => $this->baseApiUrl,
        ]);
    }

    public function getProvince(int $id = null)
    {
        $provinces = $this->getProvinces();

        if($id != null) {
            foreach($provinces as $province) {
                if($province->province_id == $id) return $province;
            }

            return [];
        }

        return $provinces;
    }

    /**
     * Return all the city data on specific province
     * @param int $pid | Province id
     * @param int $id | City id
     * 
     * @return array|object $city
     */
    public function getCity(int $pid, int $id = null)
    {
        $cities = $this->getCities($pid);

        if($id != null) {
            foreach($cities as $city) {
                if($city->city_id == $id) return $city;
            }

            return [];
        }

        return $cities;
    }

    private function getProvinces()
    {
        $path = "local/rajaongkir/provinces.json";

        if(!Storage::exists($path)) {
            $json = $this->client->request('GET', 'province', [
                'query' => [
                    'key' => $this->apiKey,
                ]
            ])->getBody()->read($this->contentSize);

            $content = $this->saveJson($path, $json);
        } else {
            $content = Storage::get($path);
        }

        return json_decode($content);
    }

    private function getCities(int $pid)
    {
        $path = "local/rajaongkir/city_{$pid}.json";

        if(!Storage::exists($path)) {
            $json = $this->client->request('GET', 'city', [
                'query' => [
                    'key' => $this->apiKey,
                    'province' => $pid,
                ]
            ])->getBody()->read($this->contentSize);

            $content = $this->saveJson($path, $json);
        } else {
            $content = Storage::get($path);
        }

        return json_decode($content);
    }

    private function saveJson(string $path, string $json)
    {
        $content = json_decode($json);

        if($content->rajaongkir->status->code == 200) {
            $encoded = json_encode($content->rajaongkir->results);

            Storage::put($path, $encoded);
            return $encoded;
        }

        throw new ErrorException("Problem with Rajaongkir API");
    }
}