<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class Product
{
    protected $dir;

    protected $path;

    protected $images = [];

    public function __construct(string $dir)
    {
        $this->dir = "uploads/products/$dir/";
        $this->path = public_path($this->dir);

        if(!file_exists($this->path)) {
            File::makeDirectory($this->path, 0777, true, true);
        }
    }

    public function uploadImages($images, $pid)
    {
        $index = 1;

        foreach($images as $image) {
            $ext = $image->getClientOriginalExtension();
            $name = $index . time() . '.' . $ext;
            $data = [
                'product_id' => $pid,
                'image_name' => $name,
                'image_path' => $this->path . $name,
                'image_url' => "/{$this->dir}$name",
                'image_ext' => $ext
            ];

            $image->move($this->path, $name);
            $this->images[] = $data;
            $index++;
        }

        return $this->images;
    }
}