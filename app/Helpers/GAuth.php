<?php
namespace App\Helpers;

use Google\Service\PeopleService;
use Google_Client;

class GAuth
{
    public $client = [];

    public function __construct($data)
    {
        $this->client = $this->getClient($data);
    }

    public function setToken($gcontact)
    {
        $token = [
            'access_token' => $gcontact['access_token'],
            'expires_in' => $gcontact['expires_in'],
            'refresh_token' => $gcontact['refresh_token'],
            'scope' => $gcontact['scope'],
            'token_type' => $gcontact['token_type'],
            'created' => $gcontact['created']
        ];

        $this->client->setAccessToken($token);

        if($this->client->isAccessTokenExpired()) {
            $rToken = $this->client->getRefreshToken();
            $this->client->fetchAccessTokenWithRefreshToken($rToken);

            return $gcontact->update($this->client->getAccessToken());
        }
    }

    private function getClient($data)
    {
        $client = new Google_Client([
            'application_name' => $data['application_name'],
            'client_id' => $data['client_id'],
            'client_secret' => $data['client_secret'],
            'scopes' => [
                PeopleService::CONTACTS            
            ],
            'redirect_uri' => $data['redirect_uri'],
            'access_type' => 'offline',
            'prompt' => 'select_account consent'
        ]);

        return $client;
    }
}
