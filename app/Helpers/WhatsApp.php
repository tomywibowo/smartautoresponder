<?php
namespace App\Helpers;

use App\Models\BroadcastSchedule;
use App\Models\Contact;
use App\Models\Whatsapp as ModelsWhatsapp;
use App\Models\WhatsAppBroadcast;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class WhatsApp
{
    private $client;

    public function __construct()
    {
        $this->client = new Client;
    }

    public function convertMessage(Contact $dest, $message): string
    {
        $message = $this->processSpintax($message);
        $message = $this->processMention($dest->toArray(), $message);
        
        return $message;
    }

    public function addBroadcast($sender, $dests, $data)
    {
        BroadcastSchedule::create([
            'sender' => $sender->phone_number,
            'destinations' => $dests,
            'message' => $data->message,
            'period' => $data->period,
            'due' => strtotime("{$data->date} {$data->time}")
        ]);
        
        return [
            'status' => true,
            'message' => 'Broadcast success added to schedule'
        ];
    }

    public function sendBroadcastSchedule()
    {
        $now = time();
        $dueSchedule = BroadcastSchedule::where("due", '<=', $now)->get();

        foreach($dueSchedule as $schedule) {
            // If schedule due was expired
            if($schedule->due < ($now - 60)) {
                $this->updateSchedulePeriod($schedule);
                continue;
            }

            if($schedule->destinations == '*') {
                $schedule->destinations = Contact::pluck('phone')->toArray();
            }

            $sender = ModelsWhatsapp::where('phone_number', $schedule->sender)->first();
            $send = $this->sendBroadcast($sender, $schedule->destinations, $schedule);
            $this->updateSchedulePeriod($schedule);
            Log::channel('broadcast')->info($send['message']);
        }
        
        if(count($dueSchedule) < 1) {
            Log::channel('broadcast')->info('No broadcast message to send');
        }
    }

    public function sendBroadcast(ModelsWhatsapp $sender, $dests, $data): array
    {
        $success = 0;
        $failed = 0;

        foreach($dests as $dest) {
            $dest = Contact::where('phone', $dest)->first();
            if(!$dest) continue;

            $broadcast = [
                'phone' => $dest->phone,
                'msg' => $this->convertMessage($dest, $data->message)
            ];

            $send = $this->sendMessage($sender, $broadcast);

            if($send['status']) {
                $broadcast = [
                    'sender' => $sender->phone_number,
                    'destination' => $dest->phone,
                    'message' => $broadcast['msg']
                ];

                WhatsAppBroadcast::create($broadcast);
            }

            ($send['status']) ? $success++ : $failed++;
        }

        return [
            'status' => true,
            'message' => "Broadcast message has been sent to $success contacts, $failed failed"
        ];
    }

    private function updateSchedulePeriod($schedule)
    {
        $period = $schedule->period;

        if(!$period) return $schedule->delete();

        $dueReplacement = [
            'hourly' => strtotime('+1 hour', $schedule->due),
            'daily' => strtotime('+1 day', $schedule->due),
            'weekly' => strtotime('+1 week', $schedule->due),
            'monthly' => strtotime('+1 month', $schedule->due),
            'annual' => strtotime('+1 year', $schedule->due)
        ];

        return $schedule->update(['due'=> $dueReplacement[$period]]);
    }

    public function sendMessage(ModelsWhatsapp $sender, $data): array
    {
        $response = $this->client->request('GET', $sender->api_url, [
            'query' => [
                'apikey' => $sender->api_key,
                'phone' => $data['phone'],
                'msg' => $data['msg']
            ]
        ])->getBody()->read(2048);

        $response = json_decode($response);

        if(!($response->status ?? true)) {
            Log::channel('broadcast')->info($response->message);
            return [
                'status' => false,
                'message' => $response->message
            ];
        }

        return [
            'status' => true,
            'message' => 'Message has been sent'
        ];
    }

    private function processMention(Array $dest, String $message)
    {
        return preg_replace_callback('#{(.*?)}#', function($match) use($dest) {
            return $dest[$match[1]] ?? '';
        }, $message);
    }

    private function processSpintax(String $text): string
    {
        return preg_replace_callback(
            '/\((((?>[^\(\)]+)|(?R))*)\)/x',
            array($this, 'replaceSpintax'),
            $text
        );
    }

    private function replaceSpintax(Array $text): string
    {
        $text = $this->processSpintax($text[1]);
        $parts = explode('|', $text);
        return trim($parts[array_rand($parts)]);
    }
}
