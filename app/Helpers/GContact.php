<?php
namespace App\Helpers;

use App\Models\GoogleContact;
use Google\Service\PeopleService;
use Google\Service\PeopleService\{EmailAddress, Name, Person, PhoneNumber};

class GContact
{
    public function generateAuthUri($data)
    {
        $gAuth = new GAuth($data);
        return $gAuth->client->createAuthUrl();
    }

    public function fetchAccToken(array $data, string $code)
    {
        $gAuth = new GAuth($data);
        $accToken = $gAuth->client->fetchAccessTokenWithAuthCode($code);
        
        if(array_key_exists('error', $accToken)) {
            return [
                'status'   => 'error',
                'message' => $accToken['error_description']
            ];
        }

        return [
            'status' => 'success',
            'data' => $accToken
        ];
    }

    public function saveContact($contact)
    {
        $gContacts = GoogleContact::all();

        foreach($gContacts as $gContact) {
            $gAuth = new GAuth($gContact);
            $gAuth->setToken($gContact);

            $pService = new PeopleService($gAuth->client);
            $person = new Person;

            $email = new EmailAddress;
            $email->setValue($contact->email);
            $person->setEmailAddresses($email);

            $name = new Name;
            $name->setGivenName($contact->name);
            $person->setNames($name);

            $phone = new PhoneNumber;
            $phone->setValue($contact->phone);
            $person->setPhoneNumbers($phone);

            $pService->people->createContact($person);
        }
    }
}
