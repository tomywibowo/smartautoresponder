<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_methods', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable(); // Table name for list payements (ex: payment_method_banks)
            $table->string('name')->nullable();
            $table->timestamps();
        });

        /* TABLE FOR LISTS */
        Schema::create('payment_method_banks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('payment_method_id')->unsigned();
            $table->string('bank_name')->nullable(); // 
            $table->string('bank_account')->nullable();
            $table->string('bank_number')->nullable();
            $table->timestamps();
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_method_banks');
        Schema::dropIfExists('payment_methods');
    }
}
