<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('url')->unique();
            $table->string('code')->unique();
            $table->enum('status', ['disabled', 'test-mode', 'live'])->default('live');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::create('product_images', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->string('image_name')->nullable();
            $table->string('image_path')->nullable();
            $table->string('image_url')->nullable();
            $table->string('image_ext')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_pricings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->double('regular_price', 16, 0)->nullable();
            $table->boolean('is_sale_price')->default(false);
            $table->double('sale_price', 16, 0)->nullable();
            $table->boolean('is_schedule_sale_price')->default(false);
            $table->json('schedule_sale_price')->nullable(); // [from: new Date(), to: new Date()]
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_bumps', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->string('name');
            $table->double('price');
            $table->string('title')->nullable();
            $table->string('subtitle')->nullable();
            $table->text('description')->nullable();
            $table->longText('html')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_shippings', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->integer('weight')->unsigned()->nullable();
            $table->json('curiers')->nullable(); // List for curiers : 'jne,tiki,pos'
            $table->json('ship_from')->nullable(); // {province_id: null, city_id: null}
            $table->boolean('is_free_shipping')->default(false);
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_payments', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->json('payment')->nullable(); // ID lists for payment method
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_unique_codes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->enum('type', ['decrease', 'increase'])->default('increase');
            $table->json('range')->nuproduct_pricingsllable(); // {min: 1, max: 999}
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_templates', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->enum('layout', ['right-sidebar', 'left-sidebar'])->default('right-sidebar');
            $table->string('background_color', 8)->default('#ffffff');
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_headers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->string('image')->nullable();
            $table->text('tagline')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_guarantee_seals', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->string('image')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_fields', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->boolean('required')->default(false);
            $table->string('placeholder')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_buttons', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->string('text_value')->nullable();
            $table->string('button_color')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_videos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->string('url')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_bullet_points', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->text('content')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_testimonials', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->string('image')->nullable();
            $table->string('name')->nullable();
            $table->text('content')->nullable();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::create('product_success_pages', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id')->unsigned();
            $table->enum('success_type', ['show_success_page', 'redirect_to_custome_url', 'redirect_to_whatsapp'])->default('show_success_page');
            $table->json('content'); // #Success{headline: '', video: ''} #CustomURL{url: ''} #Whatsapp{number: []}
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_success_pages');
        Schema::dropIfExists('product_testimonials');
        Schema::dropIfExists('product_bullet_points');
        Schema::dropIfExists('product_videos');
        Schema::dropIfExists('product_buttons');
        Schema::dropIfExists('product_fields');
        Schema::dropIfExists('product_guarantee_seals');
        Schema::dropIfExists('product_headers');
        Schema::dropIfExists('product_templates');
        Schema::dropIfExists('product_unique_codes');
        Schema::dropIfExists('product_payments');
        Schema::dropIfExists('product_shippings');
        Schema::dropIfExists('product_bumps');
        Schema::dropIfExists('product_pricings');
        Schema::dropIfExists('product_images');
        Schema::dropIfExists('products');
    }
}
