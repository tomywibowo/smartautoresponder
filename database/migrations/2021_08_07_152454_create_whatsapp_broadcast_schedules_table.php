<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhatsappBroadcastSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whatsapp_broadcast_schedules', function (Blueprint $table) {
            $table->id();
            $table->string('sender', 16);
            $table->text('destinations');
            $table->text('message');
            $table->enum('period', [
                'hourly', 'daily', 'weekly', 'monthly', 'annual'
            ])->nullable();
            $table->integer('due');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whatsapp_broadcast_schedules');
    }
}
