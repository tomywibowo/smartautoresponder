<?php

namespace Database\Factories;

use App\Models\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $this->faker->locale = 'id_ID';
        $phone = $this->faker->unique()->phoneNumber;

        return [
            'name' => $this->faker->name(),
            'phone' => preg_replace("/[^0-9+]/", '', $phone),
            'email' => $this->faker->unique()->safeEmail
        ];
    }
}
