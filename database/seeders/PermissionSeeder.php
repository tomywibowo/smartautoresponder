<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ["name" => "product-read", "fullname" => "Products", "guard_name" => "web", "parent_id" => null],
            ["name" => "product-create", "fullname" => "Create", "guard_name" => "web", "parent_id" => 1],
            ["name" => "product-update", "fullname" => "Update", "guard_name" => "web", "parent_id" => 1],
            ["name" => "product-delete", "fullname" => "Delete", "guard_name" => "web", "parent_id" => 1],

            ["name" => "contact-read", "fullname" => "Contacts", "guard_name" => "web", "parent_id" => null],
            ["name" => "contact-create", "fullname" => "Create", "guard_name" => "web", "parent_id" => 5],
            ["name" => "contact-update", "fullname" => "Update", "guard_name" => "web", "parent_id" => 5],
            ["name" => "contact-delete", "fullname" => "Delete", "guard_name" => "web", "parent_id" => 5],

            ["name" => "message-read", "fullname" => "Send Message", "guard_name" => "web", "parent_id" => null],
            ["name" => "message-create", "fullname" => "Create", "guard_name" => "web", "parent_id" => 9],
            ["name" => "message-update", "fullname" => "Update", "guard_name" => "web", "parent_id" => 9],
            ["name" => "message-delete", "fullname" => "Delete", "guard_name" => "web", "parent_id" => 9],

            ["name" => "broadcast-read", "fullname" => "Broadcast", "guard_name" => "web", "parent_id" => null],
            ["name" => "broadcast-create", "fullname" => "Create", "guard_name" => "web", "parent_id" => 13],
            ["name" => "broadcast-update", "fullname" => "Update", "guard_name" => "web", "parent_id" => 13],
            ["name" => "broadcast-delete", "fullname" => "Delete", "guard_name" => "web", "parent_id" => 13],

            ["name" => "whatsapp-read", "fullname" => "Whatsapp", "guard_name" => "web", "parent_id" => null],
            ["name" => "whatsapp-create", "fullname" => "Create", "guard_name" => "web", "parent_id" => 17],
            ["name" => "whatsapp-update", "fullname" => "Update", "guard_name" => "web", "parent_id" => 17],
            ["name" => "whatsapp-delete", "fullname" => "Delete", "guard_name" => "web", "parent_id" => 17],

            ["name" => "kirim-email-read", "fullname" => "Kirim.Email", "guard_name" => "web", "parent_id" => null],
            ["name" => "kirim-email-create", "fullname" => "Create", "guard_name" => "web", "parent_id" => 21],
            ["name" => "kirim-email-update", "fullname" => "Update", "guard_name" => "web", "parent_id" => 21],
            ["name" => "kirim-email-delete", "fullname" => "Delete", "guard_name" => "web", "parent_id" => 21],

            ["name" => "google-contact-read", "fullname" => "Google Contact", "guard_name" => "web", "parent_id" => null],
            ["name" => "google-contact-create", "fullname" => "Create", "guard_name" => "web", "parent_id" => 17],
            ["name" => "google-contact-update", "fullname" => "Update", "guard_name" => "web", "parent_id" => 17],
            ["name" => "google-contact-delete", "fullname" => "Delete", "guard_name" => "web", "parent_id" => 17],

            ["name" => "users-read", "fullname" => "Users", "guard_name" => "web", "parent_id" => null],
            ["name" => "users-create", "fullname" => "Create", "guard_name" => "web", "parent_id" => 21],
            ["name" => "users-update", "fullname" => "Update", "guard_name" => "web", "parent_id" => 21],
            ["name" => "users-delete", "fullname" => "Delete", "guard_name" => "web", "parent_id" => 21],

            ["name" => "role-read", "fullname" => "Roles", "guard_name" => "web", "parent_id" => null],
            ["name" => "role-create", "fullname" => "Create", "guard_name" => "web", "parent_id" => 32],
            ["name" => "role-update", "fullname" => "Update", "guard_name" => "web", "parent_id" => 32],
            ["name" => "role-delete", "fullname" => "Delete", "guard_name" => "web", "parent_id" => 32],

            ["name" => "permission-read", "fullname" => "Permission", "guard_name" => "web", "parent_id" => null],

            ["name" => "setting-read", "fullname" => "Setting", "guard_name" => "web", "parent_id" => null],
            ["name" => "setting-update", "fullname" => "Update", "guard_name" => "web", "parent_id" => 38]
        ];

        foreach ($permissions as $permission) {
            Permission::create($permission);
        }
    }
}
